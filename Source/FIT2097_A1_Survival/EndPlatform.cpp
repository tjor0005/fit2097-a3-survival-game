// Fill out your copyright notice in the Description page of Project Settings.


#include "EndPlatform.h"

// Sets default values
AEndPlatform::AEndPlatform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup the visible component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	BaseMesh->SetupAttachment(RootComponent);

	//BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	//RootComponent = BaseMesh;

	// Setup the collision box
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Volume"));
	TriggerVolume->SetupAttachment(RootComponent);

	// Setup relative location and world scale of collision box
	TriggerVolumeScale = FVector(3, 3, 0.1);
	TriggerVolumeLocation = FVector(0, 0, 8);
	TriggerVolume->SetWorldScale3D(TriggerVolumeScale);
	TriggerVolume->SetWorldLocation(TriggerVolumeLocation);
}

// Called when the game starts or when spawned
void AEndPlatform::BeginPlay()
{
	Super::BeginPlay();
	
	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &AEndPlatform::OnBoxOverlapBegin);
}

void AEndPlatform::OnBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Checks if the actor exists and is not this actor
	if (OtherActor && (OtherActor != this) && OtherComponent)
	{
		// checks if the actor is the player character
		AFIT2097_A1_SurvivalCharacter* Player = Cast<AFIT2097_A1_SurvivalCharacter>(OtherActor);
		if (Player)
		{
			// call playerWin function to win the game
			Player->PlayerWin();
		}
	}
}

// Called every frame
void AEndPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

