// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlotStructure.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FIT2097_A1_SURVIVAL_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	// name of inventory
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText InventoryName = FText::FromString("None");

	// number of inventory slots
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int NumberOfSlots = 0;

	// referenc to list of slot structures
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FSlotStructure> Inventory;

	// Add content to inventory
	bool AddToInventory(FSlotStructure Content);

	// create a new stack in inventory
	void CreateStack(FSlotStructure& Content);

	void PrepareInventory();

	//UFUNCTION(BlueprintCallable, Category = "Slot Structure")
	//	void HasPartialStack(const FSlotStructure& SlotStructure, bool& IsPartialStack, int& StackIndex);

	// checks invenotry if it contains Item in Slot Structure and stack is not full
	int HasPartialStack(FSlotStructure SlotStructure);

	// add item to current stack
	bool AddToStack(FSlotStructure SlotStructure, int SlotIndex);

	UFUNCTION(BlueprintCallable)
		void RemoveFromStack(const int SlotIndex);
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
