// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DamageableCharacter.h"

#include "Kismet/GameplayStatics.h"
#include "FIT2097_A1_SurvivalProjectile.h"
#include "Stunnable.h"
// import ai behaviour trees
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"

// import ai navigation
#include "Perception/AIPerceptionTypes.h"
#include "NavigationSystem.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISenseConfig_Damage.h"

#include "CoreMinimal.h"
#include "AIController.h"
#include "TeamAIController.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API ATeamAIController : public AAIController, public IStunnable
{
	GENERATED_BODY()
	
public:

	ATeamAIController();

	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* InPawn) override;

	virtual void Tick(float DeltaTime) override;

	// sends pointer for a particular actor i.e. the actor that has just been updated
	// not all actors that have been detected previously
	UFUNCTION()
		void OnSensesUpdated(AActor* UpdateActor, FAIStimulus Stimulus);
	// Stimulus allows us to tell whether or not the actor that has been updated has 
	// exited or entered the senses

	// How far the AI can see
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
		float SightRadius = 1000.0f;

	// How long it will retain memory of spotting the player
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
		float SightAge = 3;

	// a separate radius for once the actor has been detected how long until it is lost
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
		float SightRadiusLose = SightRadius + 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
		float FieldOfView = 90; // degree of field of view on either side

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AI")
		UAISenseConfig_Sight* SightConfiguration;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AI")
		UAISenseConfig_Damage* DamageConfiguration;

	UNavigationSystemV1* NavigationSystem;

	// reference to target to follow
	APawn* TargetEnemy;

	UPROPERTY(EditDefaultsOnly, Category = Blackboard)
		UBlackboardData* AIBlackboard;

	UPROPERTY(EditDefaultsOnly, Category = Blackboard)
		UBehaviorTree* BehaviorTree;

	UPROPERTY(EditDefaultsOnly, Category = Blackboard)
		UBlackboardComponent* BlackboardComponent;

	void SightReaction(AActor* UpdateActor, FAIStimulus Stimulus);

	void DamageReaction(AActor* UpdateActor, FAIStimulus Stimulus);

	// WE INHERIT THIS FUNCTION FROM INTERFACE STUNNABLE
	// This is not the function that will be called
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Stunnable")
		void Stun(float StunTime);
	// This is the function that will be called when Stun event is called on this object
	virtual void Stun_Implementation(float StunTime) override;

	// Counter that keeps track of how long pawn is stunned
	float StunCounter = 0.0f;

	// Denotes if enemy is stunned
	bool Stunned = false;

	void ResetChaseEnemy();
};
