// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


bool UInventoryComponent::AddToInventory(FSlotStructure Content)
{
	int SlotIndex = HasPartialStack(Content);
	// Check if content is stackable
	if (Content.ItemStructure.IsStackable && SlotIndex != INDEX_NONE)
	{
		AddToStack(Content, SlotIndex);
	} 
	else 
	{
		CreateStack(Content);
	}
	return true;
}

void UInventoryComponent::CreateStack(FSlotStructure& Content)
{
	int EmptySlotIndex = INDEX_NONE;

	// Check if inventory contains an empty slot
	for (int i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i].Quantity == 0)
		{
			EmptySlotIndex = i;
			break;
		}
	}

	// Allocate Content to empty inventory slot if it exists
	if (EmptySlotIndex != INDEX_NONE)
	{
		Inventory[EmptySlotIndex] = Content;
	}
	// Inventory is full
	else 
	{
		
	}
}

void UInventoryComponent::PrepareInventory()
{
	//Inventory.SetNum(NumberOfSlots);
	for (int i = 0; i < NumberOfSlots; i++)
	{
		struct FSlotStructure EmptySlot;
		Inventory.Add(EmptySlot);
	}
}

int UInventoryComponent::HasPartialStack(FSlotStructure SlotStructure)
{
	int InventoryIndex = INDEX_NONE;

	// check if inventory contains the same slot item
	for (int ArrayIndex = 0; ArrayIndex < Inventory.Num(); ArrayIndex++)
	{
		// checks if the slots are of the same class and if our slot stack is already at max
		if (Inventory[ArrayIndex].ItemStructure.Class == SlotStructure.ItemStructure.Class &&
			Inventory[ArrayIndex].Quantity < SlotStructure.ItemStructure.MaxStackSize)
		{
			InventoryIndex = ArrayIndex;
			break;
		}
	}
	return InventoryIndex;
}

bool UInventoryComponent::AddToStack(FSlotStructure SlotStructure, int SlotIndex)
{
	// Calculate the sum of current stack quantity and added quantity
	int CombinedQuantity = Inventory[SlotIndex].Quantity + SlotStructure.Quantity;

	// get the maximum stack size for item
	int MaxStackAmount = Inventory[SlotIndex].ItemStructure.MaxStackSize;

	// Check if sum of quantities is greater than Max Stack Size
	if (CombinedQuantity > MaxStackAmount)
	{
		// Set slot at index to max quantity
		Inventory[SlotIndex].Quantity = MaxStackAmount;

		// Set SlotStructure Quantity to amount over max stack size
		SlotStructure.Quantity = CombinedQuantity - MaxStackAmount;
		AddToInventory(SlotStructure);
	}
	// If there is enough space in slot for added item content
	else
	{
		Inventory[SlotIndex].Quantity = CombinedQuantity;
	}
	return true;
}

void UInventoryComponent::RemoveFromStack(const int SlotIndex)
{
	// checks if slot index is valid
	if (SlotIndex < 0 || SlotIndex >= Inventory.Num())
	{
		return;
	}

	// Reduces the slot quantity by one or to 0
	Inventory[SlotIndex].Quantity = FMath::Max(0, Inventory[SlotIndex].Quantity - 1);

	// Turn inventory slot empty is quantity is 0
	if (Inventory[SlotIndex].Quantity <= 0)
	{
		struct FSlotStructure EmptySlot;
		Inventory[SlotIndex] = EmptySlot;
	}

}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	PrepareInventory();
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

