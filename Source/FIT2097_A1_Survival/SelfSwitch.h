// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Displayable.h"
#include "Interactable.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SelfSwitch.generated.h"

// Create event to activate listeners
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOperateSwitches);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOperateObject, ASelfSwitch*, SwitchActor, bool, Activation);


UCLASS()
class FIT2097_A1_SURVIVAL_API ASelfSwitch : public AActor, public IInteractable, public IDisplayable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASelfSwitch();

	UPROPERTY(BlueprintAssignable)
		FOperateObject OperateObject;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sensor Switch")
		TArray<ASelfSwitch*> SensorSwitches;

	// setup mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		class UStaticMeshComponent* BaseMesh;

	// refer to switch name
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Description")
		FText Name = FText::FromString("Self Switch");

	// refer to switch description
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Description")
		FText Description = FText::FromString("Activate all self switches to solve the puzzle");

	// refer to how to interact with switch
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Description")
		FText HowTo = FText::FromString("Press E to interact");

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Displayable")
		float DisplayTime = 0.01;

	float DisplayCounter = 0;

	// Object Material
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		UMaterialInterface* Material;

	UMaterialInstanceDynamic* matInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		FLinearColor OnColour = FLinearColor(0.02569, 0.520833, 0);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		FLinearColor OffColour = FLinearColor(0.552083, 0, 0.007221);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		float LookAtEmissiveMultiplier = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		float LookAwayEmissiveMultiplier = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		float LookAtBorder = 0.0f;

	// get the information of the switch
	FText GetDescription();

	UPROPERTY(BlueprintAssignable)
		FOperateSwitches OperateSwitches;

	UFUNCTION()
		void OperateSwitch();

	bool SwitchActivated = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// WE INHERIT THIS FUNCTION FROM INTERFACE INTERACT
	// This is not the function that will be called
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
		void Interact();
	// This is the function that will be called when Interact event is called on this object
	virtual void Interact_Implementation() override;

	// WE INHERIT THIS FUNCTION FROM INTERFACE DISPLAYABLE
	// This is not the function that will be called
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Display")
		FText TextToDisplay();
	// This is the function that will be called when Display event is called on this object
	virtual FText TextToDisplay_Implementation() override;
};
