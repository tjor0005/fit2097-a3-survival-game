// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enum_Teams.generated.h"

UENUM(BlueprintType)
enum ETeams
{
	RED = 0 UMETA(DisplayName = "Red Team"),
	BLUE = 1 UMETA(DisplayName = "Blue Team"),
};