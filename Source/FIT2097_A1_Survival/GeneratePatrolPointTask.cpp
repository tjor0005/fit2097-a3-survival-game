// Fill out your copyright notice in the Description page of Project Settings.


#include "GeneratePatrolPointTask.h"

EBTNodeResult::Type UGeneratePatrolPointTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBehaviorTreeComponent* Component = &OwnerComp;

	// check if behaviour component exists
	if (!Component)
	{
		return EBTNodeResult::Failed;
	}

	// Get enemy ai controller from behaviour tree
	AAIPatrolController* MyController = Cast<AAIPatrolController>(Component->GetOwner());

	// check if behaviour tree is attached to enemy ai controller
	if (!MyController)
	{
		return EBTNodeResult::Failed;
	}

	MyController->GenerateNextPatrolPoint();

	return EBTNodeResult::Succeeded;
}