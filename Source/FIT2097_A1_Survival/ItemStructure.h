// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Item.h"

#include "CoreMinimal.h"
#include "Engine/UserDefinedStruct.h"
#include "ItemStructure.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct FItemStructure
{
    GENERATED_BODY()

        FORCEINLINE FItemStructure();

    explicit FORCEINLINE FItemStructure(FText ItemName, bool Stackable, UTexture2D* Image,
        int MaxStackAmount, FText ItemDesc, bool Consumability, float DurabilityAmount, TSubclassOf<class AItem> Item);

    // name of item
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FText Name = FText::FromString("None");

    // if the item is stackable
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        bool IsStackable = true;

    // image representing the item in the HUD
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        UTexture2D* Thumbnail = nullptr;

    // maximum stack size
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        int  MaxStackSize = 4;

    // Item description
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FText Description = FText::FromString("");

    // If item can be consumed
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        bool IsConsumable = true;

    // Durability of item
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float Durability = 0.0f;

    // Reference to Item class
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        TSubclassOf<class AItem> Class = nullptr;

    bool operator==(const FItemStructure& Other)const;
    bool operator!=(const FItemStructure& Other)const;
};

FORCEINLINE FItemStructure::FItemStructure()
{
}

FORCEINLINE FItemStructure::FItemStructure(FText ItemName, bool Stackable, UTexture2D* Image,
    int MaxStackAmount, FText ItemDesc, bool Consumability, float DurabilityAmount, TSubclassOf<class AItem> Item) : Name(ItemName),
    IsStackable(Stackable), Thumbnail(Image), MaxStackSize(MaxStackAmount), Description(ItemDesc), IsConsumable(Consumability),
    Durability(Durability), Class(Item)
{
}

FORCEINLINE bool FItemStructure::operator==(const FItemStructure& Other) const
{
    bool res = true;
    if (Name.ToString() != Other.Name.ToString())
    {
        res = false;
    }
    if (IsConsumable != Other.IsConsumable)
    {
        res = false;
    }
    if (Durability != Other.Durability)
    {
        res = false;
    }
    if (MaxStackSize != Other.MaxStackSize)
    {
        res = false;
    }
    if (Description.ToString() != Other.Description.ToString())
    {
        res = false;
    }
    //if (Thumbnail != Other.Thumbnail)
    //{
    //    res = false;
    //}
    if (IsStackable != Other.IsStackable)
    {
        res = false;
    }
    if (Class != Other.Class)
    {
        res = false;
    }
    return res;
}

FORCEINLINE bool FItemStructure::operator!=(const FItemStructure& Other) const
{
    bool res = true;
    if (Name.ToString() != Other.Name.ToString())
    {
        res = false;
    }
    if (IsConsumable != Other.IsConsumable)
    {
        res = false;
    }
    if (Durability != Other.Durability)
    {
        res = false;
    }
    if (MaxStackSize != Other.MaxStackSize)
    {
        res = false;
    }
    if (Description.ToString() != Other.Description.ToString())
    {
        res = false;
    }
    if (Thumbnail != Other.Thumbnail)
    {
        res = false;
    }
    if (IsStackable != Other.IsStackable)
    {
        res = false;
    }
    if (Class != Other.Class)
    {
        res = false;
    }
    return !res;
}