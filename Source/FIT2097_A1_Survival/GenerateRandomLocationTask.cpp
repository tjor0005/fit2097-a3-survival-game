// Fill out your copyright notice in the Description page of Project Settings.


#include "GenerateRandomLocationTask.h"

EBTNodeResult::Type UGenerateRandomLocationTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBehaviorTreeComponent* Component = &OwnerComp;

	// check if behaviour component exists
	if (!Component)
	{
		return EBTNodeResult::Failed;
	}

	// Get enemy ai controller from behaviour tree
	AAIWanderController* MyController = Cast<AAIWanderController>(Component->GetOwner());

	// check if behaviour tree is attached to enemy ai controller
	if (!MyController)
	{
		return EBTNodeResult::Failed;
	}

	MyController->GenerateNewRandomLocation();

	return EBTNodeResult::Succeeded;
}