// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthPickup.h"

AHealthPickup::AHealthPickup()
{
	Name = FText::FromString("Health Pickup");
	Description = FText::FromString("Use to Increase Health");
	OnColour = FLinearColor(1, 0, 0.007221);
}

bool AHealthPickup::UseItem()
{
	bool res = Super::UseItem();

	if (PlayerTarget)
	{
		PlayerTarget->Heal(HealthGiven);
	}

	return res;
}

