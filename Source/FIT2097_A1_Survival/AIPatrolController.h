// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TeamPatrolCharacter.h"

#include "CoreMinimal.h"
#include "TeamAIController.h"
#include "AIPatrolController.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API AAIPatrolController : public ATeamAIController
{
	GENERATED_BODY()
	
public:

	AAIPatrolController();

	void GenerateNextPatrolPoint();

};
