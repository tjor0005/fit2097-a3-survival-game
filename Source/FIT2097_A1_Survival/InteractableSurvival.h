// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InventoryComponent.h"
#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InteractableSurvival.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractableSurvival : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class FIT2097_A1_SURVIVAL_API IInteractableSurvival
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	// Interacts with actor implementing interface with access to player inventory
	// returns an FString of an item to be removed from the player inventory i.e. player item has been used to interact with actor
	// FString with '' means that no items are removed from inventory
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
		int InteractInventory(class UInventoryComponent* InventoryComponent);
};
