// Fill out your copyright notice in the Description page of Project Settings.


#include "LockedSwitch.h"

ALockedSwitch::ALockedSwitch() 
{
	Name = FText::FromString("Locked Switch");
	Description = FText::FromString("A switch that can only be opened with a certain key");
}

void ALockedSwitch::BeginPlay()
{
	Super::BeginPlay();

	// checks if the key has been initialised
}

int ALockedSwitch::InteractInventory_Implementation(UInventoryComponent* InventoryComponent)
{
	int res = INDEX_NONE;
	// Checks if key has not been initialised
	if (!Key)
	{
		changed = true;
	}
	// Key has been initialised
	else
	{
		// Go over inventory and check if it contains the key
		TArray<FSlotStructure> Inventory = InventoryComponent->Inventory;

		for (int i = 0; i < Inventory.Num(); i++)
		{
			// Get name, description and class of slot item
			FText ItemName = Inventory[i].ItemStructure.Name;
			FText ItemDescription = Inventory[i].ItemStructure.Description;
			TSubclassOf<AItem> ItemClass = Inventory[i].ItemStructure.Class;

			// check if slot description is equal to key description and classes are the same
			if (ItemName.ToString() == Key->Name.ToString() && ItemDescription.ToString() == Key->Description.ToString() && 
				ItemClass == Key->GetClass())
			{
				changed = true;
				break;
			}
		}
		// Checks if inventory does not contain key
		if (!changed)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("You Do Not Have The Right Key"));
		}
	}
	


	return res;
}

//FString ALockedSwitch::InteractInventory_Implementation(const TArray<FString>& SurvivalInventory)
//{
//	FString res = FString("");
//
//	// checks if player has been initalised
//	int Index = SurvivalInventory.Find(KeyPattern);
//	if (Index != INDEX_NONE)
//	{
//		// player has the keyppattern and thus flip the switch
//		changed = true;
//	}
//	else 
//	{
//		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("You do not have the right %s to unlock this"), *KeyType));
//	}
//
//	return res;
//}
