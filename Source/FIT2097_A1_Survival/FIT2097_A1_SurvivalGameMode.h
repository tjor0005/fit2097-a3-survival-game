// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FIT2097_A1_SurvivalGameMode.generated.h"

UCLASS(minimalapi)
class AFIT2097_A1_SurvivalGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFIT2097_A1_SurvivalGameMode();
};



