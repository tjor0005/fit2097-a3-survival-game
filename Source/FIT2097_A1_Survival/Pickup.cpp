// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup.h"

// Sets default values
APickup::APickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// initialise staic mesh
	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObject(TEXT("/Game/StarterContent/Props/MaterialSphere.MaterialSphere"));

	if (MeshObject.Succeeded())
	{
		BaseMesh->SetStaticMesh(MeshObject.Object);
	}

	// initialise Thumbnail
	ConstructorHelpers::FObjectFinder<UTexture2D> Texture(TEXT("/Game/_MyContent/Thumbnails/PNG_HealthPickup.PNG_HealthPickup"));

	if (Texture.Succeeded())
	{
		Thumbnail = Texture.Object;
	}

	IsConsumable = true;
}

void APickup::BeginPlay()
{
	Super::BeginPlay();

	// initialise PlayerTarget
	ACharacter* Character = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	PlayerTarget = Cast<AFIT2097_A1_SurvivalCharacter>(Character);
}
