// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class FIT2097_A1_Survival : ModuleRules
{
	public FIT2097_A1_Survival(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay",
		"NavigationSystem", "GameplayTasks", "Niagara"});

		PublicIncludePaths.AddRange(new string[]
		{
			"FIT2097_A1_Survival/",
			"F:/Epic Games/Games/UE_4.26/Engine/Plugins/FX/Niagara/Source/Niagara/Classes",
			"F:/Epic Games/Games/UE_4.26/Engine/Plugins/FX/Niagara/Source/Niagara/Private",
			"F:/Epic Games/Games/UE_4.26/Engine/Plugins/FX/Niagara/Source/Niagara/Public"
		});
	}
}
