// Copyright Epic Games, Inc. All Rights Reserved.

#include "FIT2097_A1_SurvivalCharacter.h"
#include "FIT2097_A1_SurvivalProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
#include "TimerManager.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AFIT2097_A1_SurvivalCharacter

void AFIT2097_A1_SurvivalCharacter::Hurt(float points)
{
	HealthPoints -= points;

	// Broadcast the player has been damage

	// Play Damage audio
	if (DamageAudio)
	{
		UGameplayStatics::PlaySoundAtLocation(this, DamageAudio, GetActorLocation());
	}

	ShowPlayerDamage.Broadcast();

	// Set TimerHandle to hide visual fc of player damage after ShowDamageTime amount of time
	GetWorldTimerManager().SetTimer(DamageTimerHandle, this, &AFIT2097_A1_SurvivalCharacter::RemovePlayerDamage, ShowDamageTime, false);
	if (HealthPoints <= MinHealthPoints)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("Player has died from starvation"));
		HealthPoints = MinHealthPoints;
		PlayerLose();
	}
}

void AFIT2097_A1_SurvivalCharacter::Heal(float points)
{
	HealthPoints += points;
	if (HealthPoints > MaxHealthPoints)
	{
		HealthPoints = MaxHealthPoints;
	}
}

void AFIT2097_A1_SurvivalCharacter::DecreaseFood(float points)
{
	FoodPoints -= points;
	if (FoodPoints <= MinWaterPoints)
	{
		FoodPoints = MinWaterPoints;
		ZeroFood = true;
	}
	UpdateAttributes();
}

void AFIT2097_A1_SurvivalCharacter::IncreaseFood(float points)
{
	FoodPoints += points;
	ZeroFood = false;
	if (FoodPoints > MaxFoodPoints)
	{
		FoodPoints = MaxFoodPoints;
	}
	UpdateAttributes();
}

void AFIT2097_A1_SurvivalCharacter::DecreaseWater(float points)
{
	WaterPoints -= points;
	if (WaterPoints <= MinWaterPoints)
	{
		WaterPoints = MinWaterPoints;
		ZeroWater = true;
	}
	UpdateAttributes();
}

void AFIT2097_A1_SurvivalCharacter::IncreaseWater(float points)
{
	WaterPoints += points;
	ZeroWater = false;
	if (WaterPoints > MaxWaterPoints)
	{
		WaterPoints = MaxWaterPoints;
	}
	UpdateAttributes();
}

void AFIT2097_A1_SurvivalCharacter::EnableAttributeDrain()
{
	GetWorldTimerManager().SetTimer(FoodTimerHandle, this, &AFIT2097_A1_SurvivalCharacter::AdvanceFoodTimer, FoodDropRate, true);
	GetWorldTimerManager().SetTimer(WaterTimerHandle, this, &AFIT2097_A1_SurvivalCharacter::AdvanceWaterTimer, WaterDropRate, true);
}

void AFIT2097_A1_SurvivalCharacter::AddDrunkEffect(float Duration)
{
	ShowDrunkEffect.Broadcast();

	// Set TimerHandle to hide visual drunk fx after Duration amount of time
	GetWorldTimerManager().SetTimer(DrunkTimerHandle, this, &AFIT2097_A1_SurvivalCharacter::RemoveDrunkEffect, Duration, false);
}

void AFIT2097_A1_SurvivalCharacter::RemoveDrunkEffect()
{
	HideDrunkEffect.Broadcast();
}

void AFIT2097_A1_SurvivalCharacter::RemovePlayerDamage()
{
	HidePlayerDamage.Broadcast();
}

AFIT2097_A1_SurvivalCharacter::AFIT2097_A1_SurvivalCharacter()
{
	// Setup Player damage sfx
	DamageAudio = LoadObject<USoundBase>(NULL, TEXT("/Game/_MyContent/Audio/PlayerDamage.PlayerDamage"), NULL, LOAD_None, NULL);

	// Setup Inventory Component
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("Inventory Component"));
	InventoryComponent->NumberOfSlots = MaxInventorySize;
	InventoryComponent->InventoryName = FText::FromString("Backpack");
	this->AddOwnedComponent(InventoryComponent);

	// Setup Trigger Capsule
	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(55.f, 96.f);
	// This has the same profile name as the health pickup trigger capsule collision
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);

	// Setup collision events to be called only when Trigger Capsule is colliding
	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &AFIT2097_A1_SurvivalCharacter::OnOverlapBegin);

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(false);			// otherwise won't be visible in the multiplayer
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(false);			// otherwise won't be visible in the multiplayer
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;
}

void AFIT2097_A1_SurvivalCharacter::PlayerLose()
{
	GameState->ChangeGameState(EGameStates::DEAD);
}

void AFIT2097_A1_SurvivalCharacter::PlayerWin()
{
	GameState->ChangeGameState(EGameStates::WON);
}

void AFIT2097_A1_SurvivalCharacter::PauseGame()
{
	GamePaused = !GamePaused;

	if (GamePaused && GameState)
	{
		GameState->ChangeGameState(EGameStates::PAUSED);
	}
	else if (GameState)
	{
		GameState->ChangeGameState(EGameStates::PLAYING);
	}
}

void AFIT2097_A1_SurvivalCharacter::UpdateAttributes()
{
	if (!ZeroFood && !ZeroWater)
	{
		GetWorldTimerManager().ClearTimer(HealthTimerHandle);
		TimerSet = false;
	} else if ((ZeroFood || ZeroWater) && !TimerSet)
	{
		TimerSet = true;
		GetWorldTimerManager().SetTimer(HealthTimerHandle, this, &AFIT2097_A1_SurvivalCharacter::AdvanceHealthTimer, HealthDropRate, true);
	}
}

void AFIT2097_A1_SurvivalCharacter::AdvanceHealthTimer()
{
	float damage = HealthDropAmount;
	if (ZeroFood && ZeroWater)
	{
		damage *= HealthDropMultiplier;
	}
	Hurt(damage);
}

void AFIT2097_A1_SurvivalCharacter::AdvanceFoodTimer()
{
	DecreaseFood(FoodDropAmount);
}

void AFIT2097_A1_SurvivalCharacter::AdvanceWaterTimer()
{
	DecreaseWater(WaterDropAmount);
}

void AFIT2097_A1_SurvivalCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}

	// add tag
	Tags.Add(FName(FString::FromInt(AllyTeam)));
}

//////////////////////////////////////////////////////////////////////////
// Input

void AFIT2097_A1_SurvivalCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind open/close inventory event
	FInputActionBinding& toggleInventory = PlayerInputComponent->BindAction("Inventory", IE_Pressed, this, &AFIT2097_A1_SurvivalCharacter::ShowInventory);
	//toggleInventory.bExecuteWhenPaused = true;

	// Bind pause game event
	FInputActionBinding& togglePause = PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &AFIT2097_A1_SurvivalCharacter::PauseGame);
	togglePause.bExecuteWhenPaused = true;

	// Bind interact event
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AFIT2097_A1_SurvivalCharacter::CallMyInteractTrace);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFIT2097_A1_SurvivalCharacter::OnFire);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AFIT2097_A1_SurvivalCharacter::OnResetVR);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AFIT2097_A1_SurvivalCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFIT2097_A1_SurvivalCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFIT2097_A1_SurvivalCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFIT2097_A1_SurvivalCharacter::LookUpAtRate);
}

void AFIT2097_A1_SurvivalCharacter::OnFire()
{
	// try and fire a projectile
	if (ProjectileClass != nullptr)
	{
		UWorld* const World = GetWorld();
		if (World != nullptr)
		{
			if (bUsingMotionControllers)
			{
				const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
				const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
				World->SpawnActor<AFIT2097_A1_SurvivalProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
			}
			else
			{
				const FRotator SpawnRotation = GetControlRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				// spawn the projectile at the muzzle
				World->SpawnActor<AFIT2097_A1_SurvivalProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			}
		}
	}

	// try and play the sound if specified
	if (FireSound != nullptr)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != nullptr)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != nullptr)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void AFIT2097_A1_SurvivalCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AFIT2097_A1_SurvivalCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AFIT2097_A1_SurvivalCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void AFIT2097_A1_SurvivalCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

void AFIT2097_A1_SurvivalCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AFIT2097_A1_SurvivalCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AFIT2097_A1_SurvivalCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFIT2097_A1_SurvivalCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool AFIT2097_A1_SurvivalCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AFIT2097_A1_SurvivalCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AFIT2097_A1_SurvivalCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AFIT2097_A1_SurvivalCharacter::TouchUpdate);
		return true;
	}
	
	return false;
}

void AFIT2097_A1_SurvivalCharacter::ShowInventory()
{
	if (GameState)
	{
		GameState->ChangeGameState(EGameStates::INVENTORY);
	}

}

void AFIT2097_A1_SurvivalCharacter::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Overlap begin"));
		if (OtherActor->GetClass()->ImplementsInterface(UInteractable::StaticClass()))
		{
			IInteractable::Execute_Interact(OtherActor);
		}
	}
}

//***************************************************************************************************
//** Trace functions - used to detect items we are looking at in the world
//***************************************************************************************************
//***************************************************************************************************

//***************************************************************************************************
//** Trace() - called by our CallMyTrace() function which sets up our parameters and passes them through
//***************************************************************************************************

bool AFIT2097_A1_SurvivalCharacter::Trace(UWorld* World, TArray<AActor*>& ActorsToIgnore, const FVector& Start, const FVector& End, FHitResult& HitOut, ECollisionChannel CollisionChannel, bool ReturnPhysMat)
{
	// The World parameter refers to our game world (map/level) 
	// If there is no World, abort
	if (!World)
	{
		return false;
	}

	// Set up our TraceParams object
	FCollisionQueryParams TraceParams(FName(TEXT("My Trace")), true, ActorsToIgnore[0]);

	// Should we simple or complex collision?
	TraceParams.bTraceComplex = true;

	// We don't need Physics materials 
	TraceParams.bReturnPhysicalMaterial = ReturnPhysMat;

	// Add our ActorsToIgnore
	TraceParams.AddIgnoredActors(ActorsToIgnore);

	// When we're debugging it is really useful to see where our trace is in the world
	// We can use World->DebugDrawTraceTag to tell Unreal to draw debug lines for our trace
	// (remove these lines to remove the debug - or better create a debug switch!)
	const FName TraceTag("MyTraceTag");
	//World->DebugDrawTraceTag = TraceTag;
	TraceParams.TraceTag = TraceTag;


	// Force clear the HitData which contains our results
	HitOut = FHitResult(ForceInit);

	// Perform our trace
	World->LineTraceSingleByChannel
	(
		HitOut,		//result
		Start,	//start
		End, //end
		CollisionChannel, //collision channel
		TraceParams
	);

	// If we hit an actor, return true
	return (HitOut.GetActor() != NULL);
}

//***************************************************************************************************
//** CallMyTrace() - sets up our parameters and then calls our Trace() function
//***************************************************************************************************

void AFIT2097_A1_SurvivalCharacter::CallMyInteractTrace()
{
	// Get the location of the camera (where we are looking from) and the direction we are looking in
	const FVector Start = FirstPersonCameraComponent->GetComponentLocation();
	const FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();

	// How far in front of our character do we want our trace to extend?
	// ForwardVector is a unit vector, so we multiply by the desired distance
	const FVector End = Start + ForwardVector * 314;

	// Force clear the HitData which contains our results
	FHitResult HitData(ForceInit);

	// What Actors do we want our trace to Ignore?
	TArray<AActor*> ActorsToIgnore;

	//Ignore the player character - so you don't hit yourself!
	ActorsToIgnore.Add(this);

	// Call our Trace() function with the paramaters we have set up
	// If it Hits anything
	if (Trace(GetWorld(), ActorsToIgnore, Start, End, HitData, ECC_Visibility, false))
	{
		// Process our HitData
		if (HitData.GetActor())
		{

			//UE_LOG(LogClass, Warning, TEXT("This a testing statement. %s"), *HitData.GetActor()->GetName());
			ProcessInteractTraceHit(HitData);

		}
		else
		{
			// The trace did not return an Actor
			// An error has occurred
			// Record a message in the error log
		}
	}
	else
	{
		// We did not hit an Actor
		//ClearPickupInfo();

	}
}

//***************************************************************************************************
//** ProcessTraceHit() - process our Trace Hit result
//***************************************************************************************************
void AFIT2097_A1_SurvivalCharacter::ProcessInteractTraceHit(FHitResult& HitOut)
{
	if (HitOut.GetActor()->GetClass()->ImplementsInterface(UInteractable::StaticClass()))
	{
		//UE_LOG(LogClass, Warning, TEXT("Actor IS Interactable!"));
		IInteractable::Execute_Interact(HitOut.GetActor());
	}
	else if (HitOut.GetActor()->GetClass()->ImplementsInterface(UObtainable::StaticClass()))
	{
		//FString ItemDescription = IObtainable::Execute_ItemDescription(HitOut.GetActor());
		////UE_LOG(LogClass, Warning, TEXT("This a testing statement. %s"), *ItemDescription);

		//bool success = AddToInventory(ItemDescription);
		//// If item has been added to inventory, destroy item
		//if (success)
		//{
		//	HitOut.GetActor()->Destroy();
		//}
		IObtainable::Execute_Obtain(HitOut.GetActor(), this);
	}
	else if (HitOut.GetActor()->GetClass()->ImplementsInterface(UInteractableSurvival::StaticClass()))
	{
		// Execute interface method
		int SlotIndex = IInteractableSurvival::Execute_InteractInventory(HitOut.GetActor(), InventoryComponent);

		// Check if item should be removed from inventory
		if (SlotIndex != INDEX_NONE)
		{
			// remove item from inventory
			InventoryComponent->RemoveFromStack(SlotIndex);
		}
	}
	else
	{
		//UE_LOG(LogClass, Warning, TEXT("Actor is NOT Interactable!"));
		//ClearPickupInfo();
	}
}


void AFIT2097_A1_SurvivalCharacter::CallMyDisplayTrace()
{
	// Get the location of the camera (where we are looking from) and the direction we are looking in
	const FVector Start = FirstPersonCameraComponent->GetComponentLocation();
	const FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();

	// How far in front of our character do we want our trace to extend?
	// ForwardVector is a unit vector, so we multiply by the desired distance
	const FVector End = Start + ForwardVector * 314;

	// Force clear the HitData which contains our results
	FHitResult HitData(ForceInit);

	// What Actors do we want our trace to Ignore?
	TArray<AActor*> ActorsToIgnore;

	//Ignore the player character - so you don't hit yourself!
	ActorsToIgnore.Add(this);

	// Call our Trace() function with the paramaters we have set up
	// If it Hits anything
	if (Trace(GetWorld(), ActorsToIgnore, Start, End, HitData, ECC_Visibility, false))
	{
		// Process our HitData
		if (HitData.GetActor())
		{
			//UE_LOG(LogClass, Warning, TEXT("This a testing statement. %s"), *HitData.GetActor()->GetName());
			ProcessDisplayTraceHit(HitData);
		}
		else
		{
			// The trace did not return an Actor
			// An error has occurred
			// Record a message in the error log
		}
	}
	else
	{
		// We did not hit an Actor
		TextToDisplay = FText::FromString("");
		//ClearPickupInfo();

	}
}

void AFIT2097_A1_SurvivalCharacter::ProcessDisplayTraceHit(FHitResult& HitOut)
{
	if (HitOut.GetActor()->GetClass()->ImplementsInterface(UDisplayable::StaticClass()))
	{
		TextToDisplay = IDisplayable::Execute_TextToDisplay(HitOut.GetActor());
		//UE_LOG(LogClass, Warning, TextToDisplay);
	}
	else
	{
		TextToDisplay = FText::FromString("");
		//ClearPickupInfo();
	}
}

void AFIT2097_A1_SurvivalCharacter::Tick(float DeltaTime)
{
	CallMyDisplayTrace();
}

float AFIT2097_A1_SurvivalCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (DamageCauser->ActorHasTag(FName(FString::FromInt(EnemyTeam))))
	{
		Hurt(Damage);
	}
	return 0.0f;
}
