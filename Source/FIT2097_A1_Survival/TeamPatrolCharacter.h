// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DamageableCharacter.h"
#include "TeamPatrolCharacter.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API ATeamPatrolCharacter : public ADamageableCharacter
{
	GENERATED_BODY()
	
public:
	ATeamPatrolCharacter();

	// increments current checkpoint by 1
	void InCurrentCheckpoint();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Path Movement")
		TArray<FVector> PatrolPoints;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Path Movement")
		int64 CurrentCheckpoint = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
