// Fill out your copyright notice in the Description page of Project Settings.


#include "Item.h"

// Sets default values
AItem::AItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// setup mesh component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	BaseMesh->SetupAttachment(RootComponent);

	// hard code mesh
	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObject(TEXT("/Game/StarterContent/Shapes/Shape_NarrowCapsule.Shape_NarrowCapsule"));
	// hard code material
	ConstructorHelpers::FObjectFinder<UMaterial> MaterialObject(TEXT("/Game/_MyContent/Materials/Interactable_Glow.Interactable_Glow"));

	if (MeshObject.Succeeded())
	{
		BaseMesh->SetStaticMesh(MeshObject.Object);
	}

	if (MaterialObject.Succeeded())
	{
		Material = MaterialObject.Object;
		BaseMesh->SetMaterial(0, Material);
	}
}

FText AItem::GetFTextDescription()
{
	FText res = FText::FromString("");

	// Checks if variables have been initialised
	if (!Name.IsEmpty() && !Description.IsEmpty() != 0 && !HowTo.IsEmpty())
	{
		// Create an FText of the entire item description
		FString string = Name.ToString() + "\n" + Description.ToString() + "\n" + HowTo.ToString();
		res = FText::FromString(string);
	}
	return res;
}


bool AItem::UseItem()
{
	return IsConsumable;
}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();

	// initialise dynamic material
	if (Material)
	{
		matInstance = UMaterialInstanceDynamic::Create(Material, this);
		BaseMesh->SetMaterial(0, matInstance);
		matInstance->SetScalarParameterValue("Emissive multiplier", LookAwayEmissiveMultiplier);
		matInstance->SetVectorParameterValue("Colour", OnColour);
	}

	// initialise item description
	// Checks if variables have been initialised
	if (!Name.IsEmpty() && !Description.IsEmpty() != 0 && !HowTo.IsEmpty())
	{
		// Create an FText of the entire item description
		FString string = Name.ToString() + "\n" + Description.ToString() + "\n" + HowTo.ToString();
		ItemInfo = FText::FromString(string);
	}
}

// Called every frame
void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (DisplayCounter > 0)
	{
		DisplayCounter = DisplayCounter - DeltaTime;
	}
	else
	{
		matInstance->SetScalarParameterValue("Emissive multiplier", LookAwayEmissiveMultiplier);
	}
}

//FString AItem::ItemDescription_Implementation()
//{
//	// output to screen that player has picked up item
//	FString string = Name.ToString();
//	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("Picked Up %s"), *string));
//
//	// Make the mesh invisible and unable to collide with any actor
//	//BaseMesh->SetVisibility(false);
//	//BaseMesh->SetCollisionProfileName(TEXT("NoCollision"));
//	//Destroy();
//	return ItemInfo.ToString();
//}

//bool AItem::CurrentlyObtainable_Implementation()
//{
//	return Pickupable;
//}

void AItem::Obtain_Implementation(AActor* Interacter)
{
	bool res = false;

	// get inventory component from actor
	UActorComponent* ActorComponent = Interacter->GetComponentByClass(UInventoryComponent::StaticClass());
	UInventoryComponent* InventoryComponent = Cast<UInventoryComponent>(ActorComponent);

	// check if actor does indeed have an inventory component
	if (InventoryComponent)
	{
		// Create Slot Structure based on Item Structure
		FSlotStructure SlotStructure = FSlotStructure();
		FItemStructure ItemStructure = FItemStructure(Name, IsStackable, Thumbnail, MaxStackSize,
			Description, IsConsumable, Durability, Class);

		// Initialise attributes of item structure


		SlotStructure.ItemStructure = ItemStructure;

		SlotStructure.Quantity = 1;

		// Add slot structure to inventory
		res = InventoryComponent->AddToInventory(SlotStructure);
	}

	// check if slot has been successfully added to inventory
	if (res)
	{
		Destroy();
	}
}

FText AItem::TextToDisplay_Implementation()
{
	//GEngine->ClearOnScreenDebugMessages();
	//GEngine->AddOnScreenDebugMessage(-1, 0.01f, FColor::Red, GetDescription());

	if (matInstance)
	{
		matInstance->SetScalarParameterValue("Emissive multiplier", LookAtEmissiveMultiplier);
		DisplayCounter = DisplayTime;
	}

	return ItemInfo;
}

