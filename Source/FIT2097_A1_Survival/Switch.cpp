// Fill out your copyright notice in the Description page of Project Settings.


#include "Switch.h"

// Sets default values
ASwitch::ASwitch()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup the visible component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	BaseMesh->SetupAttachment(RootComponent);

	// setup initial mesh
	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObject(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));
	// hard code material
	ConstructorHelpers::FObjectFinder<UMaterial> MaterialObject(TEXT("/Game/_MyContent/Materials/Interactable_Glow.Interactable_Glow"));

	if (MeshObject.Succeeded())
	{
		BaseMesh->SetStaticMesh(MeshObject.Object);
	}

	if (MaterialObject.Succeeded())
	{
		Material = MaterialObject.Object;
		BaseMesh->SetMaterial(0, Material);
	}

}

FText ASwitch::GetDescription()
{
	FText res = FText::FromString("");
	
	if (!Name.IsEmpty() && !Description.IsEmpty() != 0 && !HowTo.IsEmpty())
	{
		FString string = Name.ToString() + "\n" + Description.ToString() + "\n" + HowTo.ToString();
		res = FText::FromString(string);
	}
	return res;
}

void ASwitch::UpdateMaterial()
{
	if (Activation)
	{
		if (matInstance)
		{
			matInstance->SetVectorParameterValue("Colour", OnColour);
		}
	}
	else {
		if (matInstance)
		{
			matInstance->SetVectorParameterValue("Colour", OffColour);
		}
	}
}

// Called when the game starts or when spawned
void ASwitch::BeginPlay()
{
	Super::BeginPlay();
	
	if (Material)
	{
		matInstance = UMaterialInstanceDynamic::Create(Material, this);
		BaseMesh->SetMaterial(0, matInstance);
		matInstance->SetVectorParameterValue("Colour", OffColour);
		matInstance->SetScalarParameterValue("Border", LookAtBorder);
	}
}

// Called every frame
void ASwitch::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (DisplayCounter > 0)
	{
		DisplayCounter = DisplayCounter - DeltaTime;
	}
	else
	{
		matInstance->SetScalarParameterValue("Emissive multiplier", LookAwayEmissiveMultiplier);
	}

	if (changed)
	{
		Activation = !Activation;
		changed = false;
		UpdateMaterial();
		Operate.Broadcast(Activation);
	}
}

int ASwitch::InteractInventory_Implementation(UInventoryComponent* InventoryComponent)
{
	changed = true;
	return INDEX_NONE;
}

FText ASwitch::TextToDisplay_Implementation()
{
	if (matInstance)
	{
		matInstance->SetScalarParameterValue("Emissive multiplier", LookAtEmissiveMultiplier);
		DisplayCounter = DisplayTime;
	}

	return GetDescription();
}

