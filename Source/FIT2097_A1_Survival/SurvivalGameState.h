// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Enum_InputModes.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

#include "SurvivalPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Enum_GameStates.h"
#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "SurvivalGameState.generated.h"



/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUpdateGameState, EGameStates, GameState);

UCLASS()
class FIT2097_A1_SURVIVAL_API ASurvivalGameState : public AGameStateBase
{
	GENERATED_BODY()
public:

	ASurvivalGameState();

	UPROPERTY(BlueprintAssignable)
		FUpdateGameState ModifyGameState;

	//UPROPERTY(BlueprintReadWrite)
	//	UUserWidget* InventoryWidget;

	UPROPERTY(BlueprintReadWrite)
		TEnumAsByte<EGameStates> CurrentState = EGameStates::DEAD;

	UFUNCTION(BlueprintCallable)
		void ChangeGameState(EGameStates GameState);

	UFUNCTION(BlueprintCallable)
		void RestartGame();

	UPROPERTY(BlueprintReadWrite)
		ASurvivalPlayerController* PlayerController;
	
	//void SetInputMode(EInputModes InputMode);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

//public:
//	// Called every frame
//	virtual void Tick(float DeltaTime) override;
};
