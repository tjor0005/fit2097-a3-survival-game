// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// Niagara Specific headers
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraSystem.h"
#include "F:/Epic Games/Games/UE_4.26/Engine/Plugins/FX/Niagara/Source/Niagara/Public/NiagaraFunctionLibrary.h"
// interfaces
#include "Stunnable.h"
#include "Kismet/GameplayStatics.h"
// import sound
#include "Sound/SoundBase.h"
// import projectile movement
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/PrimitiveComponent.h"

#include "CoreMinimal.h"
#include "Grenade.h"
#include "StunGrenade.generated.h"

class USphereComponent;
class UProjectileMovementComponent;

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API AStunGrenade : public AGrenade
{
	GENERATED_BODY()
	
public:
	AStunGrenade();

	void StunSurroundingEnemies();

	void SpawnGrenade();

	void Explode();
};
