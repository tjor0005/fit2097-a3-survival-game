// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SelfSwitch.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MultipleSwitchActivatedObject.generated.h"

UCLASS()
class FIT2097_A1_SURVIVAL_API AMultipleSwitchActivatedObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMultipleSwitchActivatedObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UStaticMeshComponent* BaseMesh;

	// Object Material
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		UMaterialInterface* Material;

	UMaterialInstanceDynamic* matInstance;

	// Object Material
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		float OnEmissive = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		float OffEmissive = 0.0f;

	// start location of actor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FVector OriginLocation;

	// target location of actor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FVector TargetLocation;

	// rate at which actor moves
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MovementRate = 0.05f;

	// signifies when the actor is activated or not
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool TurnedOn = false;

	// reference to switch class to create handler event
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<ASelfSwitch*> Switches;

	// Distance tolerance
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float DistTolerance = 10.0f;

	// Initial Direction from actor to target
	FVector TargetDirection = FVector(0, 0, 0);

	// Location of the Destination
	FVector Destination = FVector(0, 0, 0);

	// hash map of switches that keeps track which states have been activated
	TMap<ASelfSwitch*, bool> SwitchActivationStates;

	UFUNCTION()
		void OperateObject(ASelfSwitch* SwitchActor, bool Activation);

	void UpdateStatus();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
