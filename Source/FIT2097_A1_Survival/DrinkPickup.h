// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "DrinkPickup.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API ADrinkPickup : public APickup
{
	GENERATED_BODY()
	
public:
	ADrinkPickup();

	// denote amount of water to give player
	UPROPERTY(EditAnywhere)
		float DrinkGiven = 0.1f;

	virtual bool UseItem() override;
};
