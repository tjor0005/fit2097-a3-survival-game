// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TeamAIController.h"
#include "AIWanderController.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API AAIWanderController : public ATeamAIController
{
	GENERATED_BODY()
	
public:
	AAIWanderController();

	void GenerateNewRandomLocation();

};
