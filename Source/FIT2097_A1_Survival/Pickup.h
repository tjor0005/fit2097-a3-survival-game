// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/GameplayStatics.h"
#include "FIT2097_A1_SurvivalCharacter.h"
#include "Item.h"
#include "Displayable.h"
#include "FIT2097_A1_SurvivalCharacter.h"
#include "Interactable.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

UCLASS()
class FIT2097_A1_SURVIVAL_API APickup : public AItem
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickup();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
		AFIT2097_A1_SurvivalCharacter* PlayerTarget;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
