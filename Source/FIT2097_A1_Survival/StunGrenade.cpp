// Fill out your copyright notice in the Description page of Project Settings.


#include "StunGrenade.h"


AStunGrenade::AStunGrenade()
{
	// Change item description
	Name = FText::FromString("Stun Grenade");
	Description = FText::FromString("Immobilizes enemies for a certain amount of time");
}

void AStunGrenade::StunSurroundingEnemies()
{
	for (int i = 0; i < SurroundingEnemies.Num(); i++)
	{
		AController* Controller = SurroundingEnemies[i]->GetController();
		// Checks if surrounding enemy implements Stunnable Interface
		if (Controller && Controller->GetClass()->ImplementsInterface(UStunnable::StaticClass()))
		{
			IStunnable::Execute_Stun(Controller, StunTime);
		}
		//SurroundingEnemies[i]->Stun(StunTime);
	}
}

void AStunGrenade::SpawnGrenade()
{
	// Get current location and look at rotation of player
	FVector PlayerLocation = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetPawnViewLocation();
	FRotator PlayerRotation = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetViewRotation();

	// spawns another StunGrenade actor in world in front of player
	AStunGrenade* tempReference = GetWorld()->SpawnActor<AStunGrenade>(AStunGrenade::StaticClass(),
		PlayerLocation + ThrowOffset, PlayerRotation);

	// throw the grenade
	tempReference->ThrowGrenade();
}

void AStunGrenade::Explode()
{
	// run explosion effects
	RunExplodeFX();

	// Stun enemies
	StunSurroundingEnemies();

	// Destroy Item
	Destroy();
}
