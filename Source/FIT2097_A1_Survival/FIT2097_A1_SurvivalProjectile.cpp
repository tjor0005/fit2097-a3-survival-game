// Copyright Epic Games, Inc. All Rights Reserved.

#include "FIT2097_A1_SurvivalProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"

AFIT2097_A1_SurvivalProjectile::AFIT2097_A1_SurvivalProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AFIT2097_A1_SurvivalProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	Tags.Add(FName(FString::FromInt(AllyTeam)));

}

void AFIT2097_A1_SurvivalProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor != nullptr && OtherActor != this)
	{
		// Damage characters
		if (OtherActor->ActorHasTag(FName(FString::FromInt(EnemyTeam))))
		{
			// Apply Damage to the actor
			UGameplayStatics::ApplyDamage(
				OtherActor,		//Actor that will be damaged
				FMath::RandRange(MinDamage, MaxDamage),		// BaseDamage
				UGameplayStatics::GetPlayerController(GetWorld(), 0),	// Controller responsible for causing damage
				this,	// actor that caused the damage
				UDamageType::StaticClass()		// Damage type
			);

			// Inform actor controllers that actor has been damaged
			UAISense_Damage::ReportDamageEvent(
				GetWorld(),
				OtherActor,
				this,
				FMath::RandRange(MinDamage, MaxDamage),
				CollisionComp->GetComponentLocation(),
				CollisionComp->GetComponentLocation());
		}
	}
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr) && OtherComp->IsSimulatingPhysics())
	{
		// Applies impulse at moveable actor object
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());

		Destroy();
	}
}

//void AFIT2097_A1_SurvivalProjectile::BeginPlay()
//{
//	// Set projectile to be part of team
//}
