// Fill out your copyright notice in the Description page of Project Settings.


#include "TeamAIController.h"

ATeamAIController::ATeamAIController()
{
	// Set the sight configuration
	SightConfiguration = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Configuration"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));

	SightConfiguration->SightRadius = SightRadius;
	SightConfiguration->LoseSightRadius = SightRadiusLose;
	SightConfiguration->PeripheralVisionAngleDegrees = FieldOfView;
	SightConfiguration->SetMaxAge(SightAge);

	// Set the teams
	// Set AI to detect different AI that are in different teams
	SightConfiguration->DetectionByAffiliation.bDetectEnemies = true;
	SightConfiguration->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfiguration->DetectionByAffiliation.bDetectNeutrals = true;

	// Set sight as the dominant sense
	//GetPerceptionComponent()->SetDominantSense(*SightConfiguration->GetSenseImplementation());
	GetPerceptionComponent()->ConfigureSense(*SightConfiguration);

	// Bind OnSenseUpdated function to OnTargetPerceptionUpdated event
	GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this, &ATeamAIController::OnSensesUpdated);

	// Set the damage configuration
	DamageConfiguration = CreateDefaultSubobject<UAISenseConfig_Damage>(TEXT("Damage Configuration"));
	DamageConfiguration->SetMaxAge(SightAge);

	GetPerceptionComponent()->SetDominantSense(*DamageConfiguration->GetSenseImplementation());

	GetPerceptionComponent()->ConfigureSense(*DamageConfiguration);

	TargetEnemy = nullptr;
}

void ATeamAIController::BeginPlay()
{
	Super::BeginPlay();

	// Checks if blackboard has been initialised
	if (!AIBlackboard)
		return;

	// check if behaviourtree has been initialised
	if (!ensure(BehaviorTree))
		return;

	UseBlackboard(AIBlackboard, BlackboardComponent);
	RunBehaviorTree(BehaviorTree);

	NavigationSystem = Cast<UNavigationSystemV1>(GetWorld()->GetNavigationSystem());
}

void ATeamAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
}

void ATeamAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Checks if target player has been set
	if (TargetEnemy)
	{
		// update player position in the blackboard
		BlackboardComponent->SetValueAsVector("PlayerPosition", TargetEnemy->GetActorLocation());
	}

	// Checks if pawn is stunned
	if (Stunned)
	{
		// Decrement counter
		StunCounter -= DeltaTime;

		// Check if pawn should return to normal
		if (StunCounter <= 0)
		{
			// clear blackboard component
			BlackboardComponent->ClearValue("IsStunned");
			Stunned = false;
		}
	}
}

void ATeamAIController::OnSensesUpdated(AActor* UpdateActor, FAIStimulus Stimulus)
{
	switch (Stimulus.Type)
	{
	case 0:
		SightReaction(UpdateActor, Stimulus);
		break;
	case 1:
		DamageReaction(UpdateActor, Stimulus);
		break;
	default:
		break;
	}

}

void ATeamAIController::SightReaction(AActor* UpdateActor, FAIStimulus Stimulus)
{
	// get pawn of UpdateActor
	APawn* TempPawn = Cast<APawn>(UpdateActor);

	// get the enemy character pawn that we are controlling
	ADamageableCharacter* AttachedPawn = Cast<ADamageableCharacter>(GetPawn());

	// Check if given pawn and attached pawn exist and if given pawn is part of the enemy team
	if (AttachedPawn && TempPawn && TempPawn->ActorHasTag(FName(FString::FromInt(AttachedPawn->EnemyTeam))))
	{
		// Get location of where the actor was sensed in the world
		//Stimulus.StimulusLocation;

		// it has been sensed and is inside our perception radius
		if (Stimulus.WasSuccessfullySensed())
		{
			// Update enemy display to be alerted
			AttachedPawn->EnemyAlert(true);

			UE_LOG(LogTemp, Warning, TEXT("Now Chasing Enemy"));

			TargetEnemy = TempPawn;
			BlackboardComponent->SetValueAsBool("ChaseEnemy", true);
			BlackboardComponent->SetValueAsVector("PlayerPosition", TargetEnemy->GetActorLocation());
		}
		// it has been previously sensed but outside of our perception radius
		// AI has lost its view on player
		else
		{
			if (AttachedPawn)
			{
				AttachedPawn->EnemyAlert(false);
			}

			UE_LOG(LogTemp, Warning, TEXT("Now NOT Chasing Enemy"));

			TargetEnemy = nullptr;
			BlackboardComponent->ClearValue("ChaseEnemy");
		}
	}
}

void ATeamAIController::DamageReaction(AActor* UpdateActor, FAIStimulus Stimulus)
{
	// get projectile class
	AFIT2097_A1_SurvivalProjectile* Projectile = Cast<AFIT2097_A1_SurvivalProjectile>(UpdateActor);

	// get player character
	ACharacter* Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	// check if damage was from player projectile
	if (Projectile && Stimulus.WasSuccessfullySensed() && Player)
	{
		// get the enemy character pawn that we are controlling
		ADamageableCharacter* EnemyPawn = Cast<ADamageableCharacter>(GetPawn());

		// Update enemy display to be alerted
		if (EnemyPawn)
		{
			EnemyPawn->EnemyAlert(true);
		}

		UE_LOG(LogTemp, Warning, TEXT("Now Chasing Player"));
		TargetEnemy = Player;
		BlackboardComponent->SetValueAsBool("ChaseEnemy", true);
		BlackboardComponent->SetValueAsVector("PlayerPosition", TargetEnemy->GetActorLocation());
	}
}

void ATeamAIController::Stun_Implementation(float StunTime)
{
	StunCounter = StunTime;
	Stunned = true;
	BlackboardComponent->SetValueAsBool("IsStunned", true);
}

void ATeamAIController::ResetChaseEnemy()
{
	// get the enemy character pawn that we are controlling
	ADamageableCharacter* AttachedPawn = Cast<ADamageableCharacter>(GetPawn());

	// Set character to be unalert
	if (AttachedPawn)
	{
		AttachedPawn->EnemyAlert(false);
	}

	UE_LOG(LogTemp, Warning, TEXT("Now NOT Chasing Enemy"));

	// Reset Blackboard component and target player
	BlackboardComponent->ClearValue("ChaseEnemy");
	TargetEnemy = nullptr;

}
