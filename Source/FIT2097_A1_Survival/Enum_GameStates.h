// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enum_GameStates.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum EGameStates 
{
	PLAYING = 0 UMETA(DisplayName="Playing"),
	PAUSED = 1 UMETA(DisplayName="Paused"),
	DEAD = 2 UMETA(DisplayName = "Dead"),
	WON = 3 UMETA(DisplayName = "Won"),
	START = 4 UMETA(DisplayName = "Start"),
	INVENTORY = 5 UMETA(DisplayName = "Inventory")
};