// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Enum_Teams.h"
#include "Perception/AISense_Damage.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/DamageType.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FIT2097_A1_SurvivalProjectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;

UCLASS(config=Game)
class AFIT2097_A1_SurvivalProjectile : public AActor
{
	GENERATED_BODY()

	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
	USphereComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* ProjectileMovement;

public:
	AFIT2097_A1_SurvivalProjectile();

	/** called when projectile hits something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

	UPROPERTY(EditAnywhere, Category = Damage)
		float MaxDamage = 0.2;

	UPROPERTY(EditAnywhere, Category = Damage)
		float MinDamage = 0.05;

	UPROPERTY(EditAnywhere, Category = "Tags")
		TEnumAsByte<ETeams> AllyTeam = ETeams::BLUE;

	UPROPERTY(EditAnywhere, Category = "Tags")
		TEnumAsByte<ETeams> EnemyTeam = ETeams::RED;

	UPROPERTY(EditAnywhere, Category = Damage)
		TSubclassOf< class UDamageType > DamageTypeClass;

//protected:
	// Called when the game starts or when spawned
	//virtual void BeginPlay() override;
};

