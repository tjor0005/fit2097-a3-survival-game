// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerBox.h"

APowerBox::APowerBox()
{
	Name = FText::FromString("Power Box");
	Description = FText::FromString("Needs the correct fuse to power it up again");
}

int APowerBox::InteractInventory_Implementation(UInventoryComponent* InventoryComponent)
{
	
	int res = INDEX_NONE;
	// Checks if key has not been initialised
	if (!Key)
	{
		changed = true;
	}
	// Key has been initialised
	else
	{
		// Go over inventory and check if it contains the key
		TArray<FSlotStructure> Inventory = InventoryComponent->Inventory;

		for (int i = 0; i < Inventory.Num(); i++)
		{
			// Get name, description and class of slot item
			FText ItemName = Inventory[i].ItemStructure.Name;
			FText ItemDescription = Inventory[i].ItemStructure.Description;
			TSubclassOf<AItem> ItemClass = Inventory[i].ItemStructure.Class;

			// check if slot description is equal to key description and classes are the same
			if (ItemName.ToString() == Key->Name.ToString() && ItemDescription.ToString() == Key->Description.ToString() &&
				ItemClass == Key->GetClass())
			{
				changed = true;
				// set res to i to remove item slot from index i
				res = i;
				break;
			}
		}
	}


	return res;

	return INDEX_NONE;
}
