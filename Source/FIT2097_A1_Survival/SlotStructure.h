// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ItemStructure.h"

#include "CoreMinimal.h"
#include "Engine/UserDefinedStruct.h"
#include "SlotStructure.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct FSlotStructure
{
	GENERATED_BODY()

		FORCEINLINE FSlotStructure();

	explicit FORCEINLINE FSlotStructure(FItemStructure ItemStruct, int Amount);

	// reference to Item structure
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FItemStructure ItemStructure;

	// Current number of the same item in inventory slot
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Quantity = 0;

	bool operator==(const FSlotStructure& Other)const;
	bool operator!=(const FSlotStructure& Other)const;
};

FORCEINLINE FSlotStructure::FSlotStructure()
{
}

FORCEINLINE FSlotStructure::FSlotStructure(FItemStructure ItemStruct, int Amount) : ItemStructure(ItemStruct), Quantity(Amount)
{
}

FORCEINLINE bool FSlotStructure::operator==(const FSlotStructure& Other)const
{
	bool res = true;
	if (Quantity != Other.Quantity)
	{
		res = false;
	}
	if (ItemStructure == Other.ItemStructure)
	{
		res = false;
	}
	return res;
}

FORCEINLINE bool FSlotStructure::operator!=(const FSlotStructure& Other)const
{
	bool res = true;
	if (Quantity != Other.Quantity)
	{
		res = false;
	}
	if (ItemStructure == Other.ItemStructure)
	{
		res = false;
	}
	return !res;
}