// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Enum_Teams.h"
// Niagara Specific headers
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraSystem.h"
#include "F:/Epic Games/Games/UE_4.26/Engine/Plugins/FX/Niagara/Source/Niagara/Public/NiagaraFunctionLibrary.h"
// interfaces
#include "Stunnable.h"
#include "Kismet/GameplayStatics.h"
// import sound
#include "Sound/SoundBase.h"
// import projectile movement
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/PrimitiveComponent.h"

#include "CoreMinimal.h"
#include "Item.h"
#include "Grenade.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API AGrenade : public AItem
{
	GENERATED_BODY()
	
public:
	AGrenade();

	// Enemy Team to attack
	UPROPERTY(VisibleAnywhere, Category = "Tags")
		TEnumAsByte<ETeams> EnemyTeam = ETeams::RED;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		FLinearColor ExplodeColour = FLinearColor(0.552083, 0, 0.007221);

	// Audio for shooting
	UPROPERTY(EditAnywhere, Category = Audio)
		USoundBase* ExplodeAudio;

	// Sphere collision component
	UPROPERTY(EditAnywhere, Category = "Collision")
		USphereComponent* CollisionComp;

	// Size of collision component
	UPROPERTY(EditAnywhere, Category = "Collision")
		float CollisionRadius = 1000.0f;

	// How long the projectile stays in the world for
	UPROPERTY(EditAnywhere, Category = "Projectile")
		float Lifespan;

	// Velocity of projectile when it is launched
	UPROPERTY(EditAnywhere, Category = "Projectile")
		float InitialVelocity;

	// determines if the stun grenade has been thrown by the player
	bool Thrown = false;

	UPROPERTY(EditAnywhere, Category = Target)
		TArray<APawn*> SurroundingEnemies;

	// denotes the amount of time targets should be stunned for
	UPROPERTY(EditAnywhere, Category = Target)
		float StunTime = 5.0f;

	// Uses the functionality of the item
	// returns true if the item should be consumed after use. Else false
	virtual bool UseItem() override;

	UPROPERTY(EditAnywhere, Category = Location)
		FVector ThrowOffset = FVector(100.0f, 0.0f, 0.0f);

	UPROPERTY(EditAnywhere, Category = Particles)
		UNiagaraSystem* ExplodeParticles;

	// method to spawn grenade
	virtual void SpawnGrenade();

	// makes the grenade moveable
	void ThrowGrenade();

	// Method to make the grenade explode
	virtual void Explode();

	// show explosion effects
	void RunExplodeFX();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		virtual void OnSphereOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
			int32 OtherBodyIndex);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
