// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ItemStructure.h"
#include "Obtainable.h"

#include "CoreMinimal.h"
#include "Displayable.h"

#include "FIT2097_A1_SurvivalCharacter.h"
#include "GameFramework/Actor.h"
#include "Item.generated.h"

struct FItemStructure;

UCLASS()
class FIT2097_A1_SURVIVAL_API AItem : public AActor, public IDisplayable, public IObtainable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

	// mesh component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interact")
		class UStaticMeshComponent* BaseMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		UMaterialInterface* Material;

	UMaterialInstanceDynamic* matInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		FLinearColor OnColour = FLinearColor(0.494792, 0.438491, 0.054118);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		float LookAtEmissiveMultiplier = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		float LookAwayEmissiveMultiplier = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Displayable")
		float DisplayTime = 0.01;

	float DisplayCounter = 0;

	// description information about item
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Structure")
		FText Name = FText::FromString("Item");

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Structure")
		FText Description = FText::FromString("Description");

	// if the item is stackable
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item Structure")
		bool IsStackable = true;

	// image representing the item in the HUD
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item Structure")
		UTexture2D* Thumbnail;

	// maximum stack size
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item Structure")
		int  MaxStackSize = 4;

	// If item can be consumed
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item Structure")
		bool IsConsumable = true;

	// Durability of item
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item Structure")
		float Durability = 0.0f;

	// Reference to Item class
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item Structure")
		TSubclassOf<class AItem> Class = this->GetClass();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Description")
		FText HowTo = FText::FromString("Press E to pickup");

	FText ItemInfo;

	FText GetFTextDescription();

	// Uses the functionality of the item
	// returns true if the item should be consumed after use. Else false
	UFUNCTION(BlueprintCallable)
		virtual bool UseItem();
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// WE INHERIT THIS FUNCTION FROM INTERFACE OBTAINABLE
	// This is not the function that will be called
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Obtainable")
		void Obtain(AActor* Interacter);
	// This is the function that will be called when CurrentlyObtainable event is called on this object
	virtual void Obtain_Implementation(AActor* Interacter) override;

	//// WE INHERIT THIS FUNCTION FROM INTERFACE OBTAINABLE
	//// This is not the function that will be called
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Obtainable")
	//	FString ItemDescription();
	//// This is the function that will be called when ItemDescription event is called on this object
	//virtual FString ItemDescription_Implementation() override;

	//// WE INHERIT THIS FUNCTION FROM INTERFACE OBTAINABLE
	//// This is not the function that will be called
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Obtainable")
	//	bool CurrentlyObtainable();
	//// This is the function that will be called when CurrentlyObtainable event is called on this object
	//virtual bool CurrentlyObtainable_Implementation() override;

	// WE INHERIT THIS FUNCTION FROM INTERFACE DISPLAYABLE
	// This is not the function that will be called
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Displayable")
		FText TextToDisplay();
	// This is the function that will be called when Display event is called on this object
	virtual FText TextToDisplay_Implementation() override;
};
