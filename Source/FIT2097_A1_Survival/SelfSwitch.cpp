// Fill out your copyright notice in the Description page of Project Settings.


#include "SelfSwitch.h"

// Sets default values
ASelfSwitch::ASelfSwitch()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup the visible component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	BaseMesh->SetupAttachment(RootComponent);

	// setup initial mesh
	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObject(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));
	// hard code material
	ConstructorHelpers::FObjectFinder<UMaterial> MaterialObject(TEXT("/Game/_MyContent/Materials/Interactable_Glow.Interactable_Glow"));

	// attach static mesh and material to actor
	if (MeshObject.Succeeded())
	{
		BaseMesh->SetStaticMesh(MeshObject.Object);
	}

	if (MaterialObject.Succeeded())
	{
		Material = MaterialObject.Object;
		BaseMesh->SetMaterial(0, Material);
	}
}

FText ASelfSwitch::GetDescription()
{
	FText res = FText::FromString("");

	if (!Name.IsEmpty() && !Description.IsEmpty() != 0 && !HowTo.IsEmpty())
	{
		// concatenate actor information into one string
		FString string = Name.ToString() + "\n" + Description.ToString() + "\n" + HowTo.ToString();

		// convert string data type from FString to FText
		res = FText::FromString(string);
	}
	return res;
}

void ASelfSwitch::OperateSwitch()
{
	// Flip the switch of this actor
	SwitchActivated = !SwitchActivated;

	OperateObject.Broadcast(this, SwitchActivated);
}

// Called when the game starts or when spawned
void ASelfSwitch::BeginPlay()
{
	Super::BeginPlay();
	
	// check if material is initialised
	if (Material)
	{
		// create dynamic material instance and attach it to mesh
		matInstance = UMaterialInstanceDynamic::Create(Material, this);
		BaseMesh->SetMaterial(0, matInstance);

		// modify dynamic material parameters
		matInstance->SetVectorParameterValue("Colour", OffColour);
		matInstance->SetScalarParameterValue("Border", LookAtBorder);
	}

	// setup self switch listener event
	for (int i = 0; i < SensorSwitches.Num(); i++)
	{
		ASelfSwitch* SensorSwitch = SensorSwitches[i];
		if (SensorSwitch)
		{
			SensorSwitch->OperateSwitches.AddDynamic(this, &ASelfSwitch::OperateSwitch);
		}
	}
	
	OperateObject.Broadcast(this, SwitchActivated);

}

// Called every frame
void ASelfSwitch::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// check if actor should still display effects i.e. player is still looking at actor
	if (DisplayCounter > 0)
	{
		DisplayCounter = DisplayCounter - DeltaTime;
	}
	else
	{
		// reset dynamic material parameter
		matInstance->SetScalarParameterValue("Emissive multiplier", LookAwayEmissiveMultiplier);
	}

	// check if the switch is turned on or off
	if (SwitchActivated)
	{
		// modify colour parameter to on colour
		if (matInstance)
		{
			matInstance->SetVectorParameterValue("Colour", OnColour);
		}
	}
	else
	{
		// modify colour parameter to off colour
		if (matInstance)
		{
			matInstance->SetVectorParameterValue("Colour", OffColour);
		}
	}
}

void ASelfSwitch::Interact_Implementation()
{
	OperateSwitches.Broadcast();
}

FText ASelfSwitch::TextToDisplay_Implementation()
{
	// modify dynamic material to indicate player is looking at actor
	if (matInstance)
	{
		matInstance->SetScalarParameterValue("Emissive multiplier", LookAtEmissiveMultiplier);
		DisplayCounter = DisplayTime;
	}

	return GetDescription();
}

