// Fill out your copyright notice in the Description page of Project Settings.


#include "DrinkPickup.h"

ADrinkPickup::ADrinkPickup() 
{
	Name = FText::FromString("Drink Pickup");
	Description = FText::FromString("Use to Increase Water Level");
	OnColour = FLinearColor(0.008875, 0, 0.916667);

	// initialise Thumbnail
	ConstructorHelpers::FObjectFinder<UTexture2D> Texture(TEXT("/Game/_MyContent/Thumbnails/PNG_DrinkPickup.PNG_DrinkPickup"));

	if (Texture.Succeeded())
	{
		Thumbnail = Texture.Object;
	}
}

bool ADrinkPickup::UseItem()
{
	bool res = Super::UseItem();

	if (PlayerTarget)
	{
		PlayerTarget->IncreaseWater(DrinkGiven);
	}

	return res;
}
