// Fill out your copyright notice in the Description page of Project Settings.


#include "AIPatrolController.h"

AAIPatrolController::AAIPatrolController()
{

}

void AAIPatrolController::GenerateNextPatrolPoint()
{
	// Checks if navigation system has been initialised
	if (!NavigationSystem)
	{
		return;
	}

	FVector CurrentLocation = GetPawn()->GetActorLocation();
	FVector NextLocation = CurrentLocation;
	FNavLocation NavLocation;

	// check if patrol points contains checkpoints
	ATeamPatrolCharacter* PatrolCharacter = Cast<ATeamPatrolCharacter>(GetPawn());
	if (PatrolCharacter && PatrolCharacter->PatrolPoints.Num() > 0)
	{
		PatrolCharacter->InCurrentCheckpoint();
		int64 NextCheckpoint = PatrolCharacter->CurrentCheckpoint;
		NextLocation = PatrolCharacter->PatrolPoints[NextCheckpoint];
	}

	// send location to blackboard
	BlackboardComponent->SetValueAsVector("PatrolPoint", NextLocation);
}