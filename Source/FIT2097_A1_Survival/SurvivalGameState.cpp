// Fill out your copyright notice in the Description page of Project Settings.
#include "SurvivalGameState.h"
#include "Kismet/GameplayStatics.h"

ASurvivalGameState::ASurvivalGameState() : Super()
{

}

void ASurvivalGameState::ChangeGameState(EGameStates GameState)
{
	// Checks if the game is already in that game state
	if (GameState != CurrentState)
	{
		// Change GameState
		CurrentState = GameState;

		bool PauseGame = false;
		// check the new current state
		switch (CurrentState)
		{
		case PLAYING:
			break;
		case PAUSED:
			// pause the game
			PauseGame = true;
			break;
		case DEAD:
			// pause the game
			PauseGame = true;
			break;
		case WON:
			// pause the game
			PauseGame = true;
			break;
		case START:
			// pause the game
			PauseGame = true;
		case INVENTORY:
		default:
			break;
		}

		// Focus user inputs to UI or Game and UI
		// check if input mode should be UI only
		//SetInputMode(InputMode);

		// pause or unpause the game
		UGameplayStatics::SetGamePaused(this, PauseGame);

		// show or not show mouse cursor
		//if (PlayerController)
		//{
		//	//PlayerController->bShowMouseCursor = MouseVisibility;
		//}
		ModifyGameState.Broadcast(CurrentState);
	}
}

void ASurvivalGameState::RestartGame()
{
	FString Level = UGameplayStatics::GetCurrentLevelName(GetWorld());
	UGameplayStatics::OpenLevel(GetWorld(), FName(Level));
}

//void ASurvivalGameState::SetInputMode(EInputModes InputMode)
//{
//	if (PlayerController == nullptr || InventoryWidget == nullptr)
//	{
//		UE_LOG(LogClass, Warning, TEXT("SetInputMode: Player Controller and/or InventoryWidget not initialised"));
//		return;
//	}
//	switch (InputMode)
//	{
//	case GAME_ONLY:
//		UWidgetBlueprintLibrary::SetInputMode_GameOnly(PlayerController);
//		break;
//	case UI_ONLY:
//		UWidgetBlueprintLibrary::SetInputMode_UIOnly(PlayerController, InventoryWidget, true);
//		break;
//	case GAME_UI:
//		UWidgetBlueprintLibrary::SetInputMode_GameAndUI(PlayerController);
//		break;
//	default:
//		break;
//	}
//}

void ASurvivalGameState::BeginPlay()
{
	Super::BeginPlay();

	ChangeGameState(EGameStates::START);
}