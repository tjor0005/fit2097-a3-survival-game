// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Sound/SoundBase.h"
#include "TimerManager.h"
#include "Enum_Teams.h"
#include "InventoryComponent.h"
#include "SurvivalGameState.h"
#include "Obtainable.h"
#include "Item.h"
#include "Displayable.h"
#include "Interactable.h"
#include "InteractableSurvival.h"
#include "Components/CapsuleComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetSystemLibrary.h"
#include "FIT2097_A1_SurvivalCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UMotionControllerComponent;
class UAnimMontage;
class USoundBase;

// Create event to display player damage listeners
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FShowPlayerDamage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHidePlayerDamage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FShowDrunkEffect);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHideDrunkEffect);

UCLASS(config=Game)
class AFIT2097_A1_SurvivalCharacter : public ACharacter
{
	GENERATED_BODY()

	// Set up a collision check
	UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
		class UCapsuleComponent* TriggerCapsule;

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* FP_Gun;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USceneComponent* FP_MuzzleLocation;

	/** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* VR_Gun;

	/** Location on VR gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USceneComponent* VR_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* L_MotionController;

public:

	UFUNCTION(BlueprintCallable)
		void EnableAttributeDrain();

	UPROPERTY(BlueprintAssignable)
		FShowDrunkEffect ShowDrunkEffect;

	UPROPERTY(BlueprintAssignable)
		FHideDrunkEffect HideDrunkEffect;

	FTimerHandle DrunkTimerHandle;

	// Broadcast player drunk effect
	void AddDrunkEffect(float Duration);

	// Broacast player drunk effect removed
	void RemoveDrunkEffect();

	UPROPERTY(BlueprintAssignable)
		FShowPlayerDamage ShowPlayerDamage;

	UPROPERTY(BlueprintAssignable)
		FHidePlayerDamage HidePlayerDamage;

	UPROPERTY(EditAnywhere, Category = "Damage")
		float ShowDamageTime = 0.5;

	UPROPERTY(EditAnywhere, Category = "Audio")
		USoundBase* DamageAudio;

	FTimerHandle DamageTimerHandle;

	void RemovePlayerDamage();

	// Setup player ally and enemy teams
	UPROPERTY(VisibleAnywhere, Category = "Tags")
		TEnumAsByte<ETeams> AllyTeam = ETeams::BLUE;

	UPROPERTY(VisibleAnywhere, Category = "Tags")
		TEnumAsByte<ETeams> EnemyTeam = ETeams::RED;

	// Set up a collision check
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Inventory")
		class UInventoryComponent* InventoryComponent;

	AFIT2097_A1_SurvivalCharacter();

	// runs when the player has lost
	void PlayerLose();

	// is called when the player has won
	void PlayerWin();

	void PauseGame();

	bool GamePaused = false;

	// function is called to show character inventory to viewport
	void ShowInventory();

	// Create reference to Survival Game State
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "GameState")
		ASurvivalGameState* GameState;

	const int MaxInventorySize = 16;

	// Text used to diaply item info
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Info")
		FText TextToDisplay;

	// used to check current level of food and water and modifies health rate accordingly
	void UpdateAttributes();

	// represent player health drop rate
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float HealthDropRate = 1.0f;

	// represent player health drop multiplier
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float HealthDropMultiplier = 2.0f;

	// represent player health drop rate
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float HealthDropAmount = 0.01f;

	bool ZeroFood = false;
	bool ZeroWater = false;
	bool TimerSet = false;

	void AdvanceHealthTimer();
	//void CountdownHasFinished();

	// Timer Handle for health drop rate 
	FTimerHandle HealthTimerHandle;

	// represent player food drop rate
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float FoodDropRate = 1.0f;

	// represent player food drop amount
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float FoodDropAmount = 0.01f;

	void AdvanceFoodTimer();

	// Timer Handle for food drop rate 
	FTimerHandle FoodTimerHandle;

	// represent player water drop rate
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float WaterDropRate = 1.0f;

	// represent player water drop amount
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float WaterDropAmount = 0.01f;

	void AdvanceWaterTimer();

	// Timer Handle for water drop rate 
	FTimerHandle WaterTimerHandle;


	// represent player health level
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Properties")
		float HealthPoints = 1.0f;

	// maximum health points
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float MaxHealthPoints = 1.0f;

	// minimum health points
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float MinHealthPoints = 0.0f;

	// represent player food level
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Properties")
		float FoodPoints = 1.0f;

	// represent player max food level
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float MaxFoodPoints = 1.0f;

	// represent player minimum food level
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float MinFoodPoints = 0.0f;

	// represent player water level
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Properties")
		float WaterPoints = 1.0f;

	// represent player max water level
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float MaxWaterPoints = 1.0f;

	// represent player minimum water level
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float MinWaterPoints = 0.0f;

	// reduce player health
	void Hurt(float points);

	// increase player health
	void Heal(float points);

	// reduce hunger level
	void DecreaseFood(float points);

	// increase hunger level
	void IncreaseFood(float points);

	// increase water level
	void DecreaseWater(float points);

	// increase water level
	void IncreaseWater(float points);

protected:
	virtual void BeginPlay();

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AFIT2097_A1_SurvivalProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	UAnimMontage* FireAnimation;

	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	uint8 bUsingMotionControllers : 1;

protected:
	
	/** Fires a projectile. */
	void OnFire();

	/** Resets HMD orientation and position in VR. */
	void OnResetVR();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	struct TouchData
	{
		TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);
	TouchData	TouchItem;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

public:
	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	bool Trace(
		UWorld* World,
		TArray<AActor*>& ActorsToIgnore,
		const FVector& Start,
		const FVector& End,
		FHitResult& HitOut,
		ECollisionChannel CollisionChannel,
		bool ReturnPhysMat
	);

	void CallMyInteractTrace();

	void ProcessInteractTraceHit(FHitResult& HitOut);

	void CallMyDisplayTrace();

	void ProcessDisplayTraceHit(FHitResult& HitOut);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
};

