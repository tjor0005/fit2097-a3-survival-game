// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIPatrolController.h"

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "GeneratePatrolPointTask.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API UGeneratePatrolPointTask : public UBTTaskNode
{
	GENERATED_BODY()
	
	// OwnerComp = Behaviour tree that called this component
	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
