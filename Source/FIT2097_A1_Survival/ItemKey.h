// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "ItemKey.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API AItemKey : public AItem
{
	GENERATED_BODY()
	
public:
	AItemKey();
};
