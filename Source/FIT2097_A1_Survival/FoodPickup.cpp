// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodPickup.h"

AFoodPickup::AFoodPickup()
{
	Name = FText::FromString("Food Pickup");
	Description = FText::FromString("Use to Increase Food Level");
	OnColour = FLinearColor(0.15625, 0.078125, 0);

	// initialise Thumbnail
	ConstructorHelpers::FObjectFinder<UTexture2D> Texture(TEXT("/Game/_MyContent/Thumbnails/PNG_FoodPickup.PNG_FoodPickup"));

	if (Texture.Succeeded())
	{
		Thumbnail = Texture.Object;
	}
}

bool AFoodPickup::UseItem()
{
	bool res = Super::UseItem();

	if (PlayerTarget)
	{
		PlayerTarget->IncreaseFood(FoodGiven);
	}

	return res;
}
