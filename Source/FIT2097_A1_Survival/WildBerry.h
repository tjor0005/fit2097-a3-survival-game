// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "WildBerry.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API AWildBerry : public APickup
{
	GENERATED_BODY()
	
public:
	AWildBerry();

	// denote amount of health to give player
	UPROPERTY(EditAnywhere)
		float Duration = 10.0f;

	// denote amount of health to give player
	UPROPERTY(EditAnywhere)
		float AttributeBoost = 0.2f;
	
	virtual bool UseItem() override;
};
