// Fill out your copyright notice in the Description page of Project Settings.


#include "Grenade.h"

AGrenade::AGrenade()
{
	// Hard code mesh and material 
	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObject(TEXT("/Game/StarterContent/Shapes/Shape_NarrowCapsule.Shape_NarrowCapsule"));

	// setup thumbnail
	ConstructorHelpers::FObjectFinder<UTexture2D> Texture(TEXT("/Game/_MyContent/Thumbnails/StunGrenadeThumbnailV2.StunGrenadeThumbnailV2"));
	ExplodeAudio = LoadObject<USoundBase>(NULL, TEXT("/Game/_MyContent/Audio/StunExplosion.StunExplosion"), NULL, LOAD_None, NULL);

	// Attach mesh to visible component
	if (MeshObject.Succeeded())
	{
		BaseMesh->SetStaticMesh(MeshObject.Object);
	}

	// initialise thumbnail
	if (Texture.Succeeded())
	{
		Thumbnail = Texture.Object;
	}

	// Set Scale of the static mesh
	BaseMesh->SetWorldScale3D(FVector(0.5, 0.5, 0.5));

	// Set up collision sphere of actor
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));

	// intialise size
	CollisionComp->InitSphereRadius(CollisionRadius);
	CollisionComp->SetWorldLocation(FVector(0, 0, 55));
	CollisionComp->SetupAttachment(BaseMesh);
	CollisionComp->SetCollisionProfileName(TEXT("Trigger"));

	// Initialise Attributes
	// Die after 3 seconds by default
	Lifespan = 3.0f;
	InitialVelocity = 2000;
	IsConsumable = true;

	// Change item description
	Name = FText::FromString("Party Grenade");
	Description = FText::FromString("Only used as a party item");

	// Initialise Niagara system
	ExplodeParticles = LoadObject<UNiagaraSystem>(nullptr, TEXT("NiagaraSystem'/Game/_MyContent/NiagaraParticles/ExplodeSystem.ExplodeSystem'"),
		nullptr, LOAD_None, nullptr);

	OnColour = FLinearColor(0.02569, 0.520833, 0);
}

bool AGrenade::UseItem()
{
	bool res = Super::UseItem();

	SpawnGrenade();

	return res;
}

void AGrenade::SpawnGrenade()
{
	// Get current location and look at rotation of player
	FVector PlayerLocation = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetPawnViewLocation();
	FRotator PlayerRotation = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetViewRotation();

	// spawns another StunGrenade actor in world in front of player
	AGrenade* tempReference = GetWorld()->SpawnActor<AGrenade>(AGrenade::StaticClass(),
		PlayerLocation + ThrowOffset, PlayerRotation);

	// throw the grenade
	tempReference->ThrowGrenade();
}

void AGrenade::ThrowGrenade()
{
	// Manipulate object to simulate physics and overlap pawn
	UPrimitiveComponent* object = FindComponentByClass<UPrimitiveComponent>();
	object->SetSimulatePhysics(true);
	//object->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	// Add an impulse that pushes the projectile forward
	FVector direction = GetActorForwardVector();
	object->AddImpulse(direction * InitialVelocity, NAME_None, true);

	// Attach overlap event to component
	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &AGrenade::OnSphereOverlapBegin);
	CollisionComp->OnComponentEndOverlap.AddDynamic(this, &AGrenade::OnSphereEndOverlap);

	Thrown = true;
}

void AGrenade::Explode()
{
	// run explosion effects
	RunExplodeFX();

	// Destroy Item
	Destroy();
}

void AGrenade::RunExplodeFX()
{
	// Play Explode Audio
	if (ExplodeAudio)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ExplodeAudio, CollisionComp->GetComponentLocation());
	}

	// Spawn particle system
	if (ExplodeParticles)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation
		(
			GetWorld(),
			ExplodeParticles,
			CollisionComp->GetComponentLocation(), // Position
			FRotator(1), // Rotation
			FVector(1, 1, 1), // Scale
			true,
			true,
			ENCPoolMethod::AutoRelease,
			true
		);
	}
}

void AGrenade::BeginPlay()
{
	Super::BeginPlay();

	// set metallic material parameter to 0.5
	matInstance->SetScalarParameterValue("Metallic", 0.5);
}

void AGrenade::OnSphereOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr && OtherActor != this)
	{
		// Checks if the OtherActor is an enemy pawn
		APawn* Enemy = Cast<APawn>(OtherActor);

		// Add enemy to list of surrounding enemies
		if (Enemy && Enemy->ActorHasTag(FName(FString::FromInt(EnemyTeam))))
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("Enemy has reached territory"));

			SurroundingEnemies.Add(Enemy);
		}
	}
}

void AGrenade::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	if (OtherActor != nullptr && OtherActor != this)
	{
		// Checks if the OtherActor is an enemy pawn
		APawn* Enemy = Cast<APawn>(OtherActor);

		// remove enemy from list of surrounding enemies
		if (Enemy && Enemy->ActorHasTag(FName(FString::FromInt(EnemyTeam))))
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("Enemy has disappeared from territory"));

			SurroundingEnemies.Remove(Enemy);
		}
	}
}

void AGrenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Thrown)
	{
		Lifespan -= DeltaTime;

		FLinearColor LerpColour = FMath::Lerp(ExplodeColour, OnColour, Lifespan);
		matInstance->SetVectorParameterValue("Colour", LerpColour);

		// Check if grenade should explode
		if (Lifespan <= 0)
		{
			Explode();
		}
	}
}