// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TeamSwitchable.h"
#include "TeamAIController.h"
#include "Enum_Teams.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/DamageType.h"
#include "Components/CapsuleComponent.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "DamageableCharacter.generated.h"

UCLASS()
class FIT2097_A1_SURVIVAL_API ADamageableCharacter : public ACharacter, public ITeamSwitchable
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ADamageableCharacter();

	// mesh component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		class USkeletalMeshComponent* BaseMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		UMaterialInterface* Material;

	UMaterialInstanceDynamic* matInstance;

	// Modifies material instance to display team colour
	void InitialiseTeamColour();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		FLinearColor BlueTeamColour = FLinearColor(0.008875, 0.0, 0.916667);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		FLinearColor RedTeamColour = FLinearColor(0.552083, 0.0, 0.007221);

	UPROPERTY(EditAnywhere, Category = "Tags")
		TEnumAsByte<ETeams> AllyTeam = ETeams::RED;

	UPROPERTY(EditAnywhere, Category = "Tags")
		TEnumAsByte<ETeams> EnemyTeam = ETeams::BLUE;

	// Set up a collision check
	UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
		class UCapsuleComponent* TriggerCapsule;

	// represent health level
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health Properties")
		float HealthPoints = 1.0f;

	// maximum health points
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float MaxHealthPoints = 1.0f;

	// minimum health points
	UPROPERTY(EditAnywhere, Category = "Player Properties")
		float MinHealthPoints = 0.0f;

	// reduce player health
	void Hurt(float points);

	// increase player health
	void Heal(float points);

	// Modify character movement speed
	UPROPERTY(EditAnywhere, Category = "Movement")
		float SpeedModifier = 0.9;

	// Target to attack
	UPROPERTY(VisibleAnywhere, Category = "Target")
		APawn* Target;

	// Attack range
	UPROPERTY(EditAnywhere, Category = "Target")
		float TargetRange = 200.0f;

	// Amount of damage given
	UPROPERTY(EditAnywhere, Category = "Target")
		float DamageAmount = 0.1f;
	
	// Rate of damage
	UPROPERTY(EditAnywhere, Category = "Target")
		float DamageRate = 1.0f;

	float TimeCounter = 0;

	// Setup Alert notification
	UPROPERTY(EditAnywhere, Category = "Alerts")
		UTextRenderComponent* AlertText;

	// String when character is not aware
	UPROPERTY(EditAnywhere, Category = "Alerts")
		FString UnAlertString = "";

	// String to show when character is alerted
	UPROPERTY(EditAnywhere, Category = "Alerts")
		FString AlertString = "!";

	// Size of alert
	UPROPERTY(EditAnywhere, Category = "Alerts")
		float TextSize = 125.0f;

	// Offset of alert from actor position
	UPROPERTY(EditAnywhere, Category = "Alerts")
		FVector TextOffset = FVector(0, 0, 75);

	// Displays enemy alert symbol
	void EnemyAlert(bool activate);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		virtual void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override;

	// WE INHERIT THIS FUNCTION FROM INTERFACE TeamSwitchable
	// This is not the function that will be called
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "TeamSwitchable")
		void SwitchTeams(ETeams NewTeam);
	// This is the function that will be called when SwitchTeams event is called on this object
	virtual void SwitchTeams_Implementation(ETeams NewTeam) override;
};
