// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemKey.h"

AItemKey::AItemKey()
{
	// hard code mesh
	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObject(TEXT("/Game/StarterContent/Shapes/Shape_TriPyramid.Shape_TriPyramid"));

	ConstructorHelpers::FObjectFinder<UTexture2D> Texture(TEXT("/Game/_MyContent/Thumbnails/Key_Item.Key_Item"));

	if (MeshObject.Succeeded())
	{
		BaseMesh->SetStaticMesh(MeshObject.Object);
	}

	if (Texture.Succeeded())
	{
		Thumbnail = Texture.Object;
	}

	Name = FText::FromString("Key");
	Description = FText::FromString("Maybe it can be used to unlock something...");
	HowTo = FText::FromString("Press E to pickup");

	IsConsumable = false;
}