// Fill out your copyright notice in the Description page of Project Settings.


#include "DamageableCharacter.h"

// Sets default values
ADamageableCharacter::ADamageableCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// setup character movement
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0, 5, 0);

	// Setup Alert Text
	AlertText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Alert Text"));
	AlertText->SetHorizontalAlignment(EHTA_Center);
	AlertText->SetWorldSize(TextSize);
	AlertText->SetupAttachment(RootComponent);

	AlertText->SetText(UnAlertString);
	AlertText->SetWorldLocation(GetActorLocation() + TextOffset);

	// Setup Trigger Capsule
	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(70.0f, 96.f);
	// This has the same profile name as the health pickup trigger capsule collision
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);

	// Setup collision events to be called only when Trigger Capsule is colliding
	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &ADamageableCharacter::OnOverlapBegin);
	TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &ADamageableCharacter::OnOverlapEnd);

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// hard code mesh
	ConstructorHelpers::FObjectFinder<USkeletalMesh> MeshObject(TEXT("/Game/_MyContent/Mesh/My_SK_Mannequin.My_SK_Mannequin"));
	// hard code material
	ConstructorHelpers::FObjectFinder<UMaterial> MaterialObject(TEXT("/Game/_MyContent/Materials/AIColour.AIColour"));
	BaseMesh = GetMesh();

	if (BaseMesh && MeshObject.Succeeded())
	{
		BaseMesh->SetSkeletalMesh(MeshObject.Object);
		BaseMesh->SetRelativeLocation(FVector(0, 0, -94));
		BaseMesh->SetRelativeRotation(FQuat::MakeFromEuler(FVector(0, 0, -90)));
	}

	if (BaseMesh && MaterialObject.Succeeded())
	{
		Material = MaterialObject.Object;
		BaseMesh->SetMaterial(0, Material);
	}
}

void ADamageableCharacter::InitialiseTeamColour()
{
	switch (AllyTeam)
	{
	case ETeams::BLUE:
		matInstance->SetVectorParameterValue("BodyColor", BlueTeamColour);
		break;
	case ETeams::RED:
		matInstance->SetVectorParameterValue("BodyColor", RedTeamColour);
		break;
	default:
		break;
	}
}

void ADamageableCharacter::Hurt(float points)
{
	HealthPoints -= points;
	if (HealthPoints <= MinHealthPoints)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("Character has died"));
		HealthPoints = MinHealthPoints;
	}
}

void ADamageableCharacter::Heal(float points)
{
	HealthPoints += points;
	if (HealthPoints > MaxHealthPoints)
	{
		HealthPoints = MaxHealthPoints;
	}
}

void ADamageableCharacter::EnemyAlert(bool activate)
{
	if (activate)
	{
		AlertText->SetText(AlertString);
	}
	else
	{
		AlertText->SetText(UnAlertString);
	}
}

// Called when the game starts or when spawned
void ADamageableCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	// add tag
	Tags.Add(FName(FString::FromInt(AllyTeam)));

	// change enemy character movement speed to 90% speed
	GetCharacterMovement()->MaxWalkSpeed = GetCharacterMovement()->MaxWalkSpeed * SpeedModifier;

	// initialise dynamic material
	if (Material)
	{
		matInstance = UMaterialInstanceDynamic::Create(Material, this);
		BaseMesh->SetMaterial(0, matInstance);
		InitialiseTeamColour();
	}
}

// Called every frame
void ADamageableCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// check if target has been initialised
	if (Target != nullptr)
	{
		// decrement counter
		TimeCounter -= DeltaTime;

		// check if counter is equal to 0
		if (TimeCounter <= 0)
		{
			// reset time counter
			TimeCounter += DamageRate;

			// get actor to target vector
			FVector TargetDirection = Target->GetActorLocation() - GetActorLocation();

			// check if target is within target range
			if (TargetDirection.Size() <= TargetRange)
			{

				UGameplayStatics::ApplyDamage(
					Target,        //Actor that will be damaged
					DamageAmount,        // BaseDamage
					GetController(),    // Controller responsible for causing damage
					this,    // actor that caused the damage
					UDamageType::StaticClass()        // Damage type
				);
			}
		}

	}
	// Reset timer
	else
	{
		TimeCounter = 0;
	}
}

void ADamageableCharacter::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Overlap begin"));
		APawn* target = Cast<APawn>(OtherActor);

		// Checks if actor exists and if they are the enemy team
		if (target && OtherActor->ActorHasTag(FName(FString::FromInt(EnemyTeam))))
		{
			Target = target;
		}
	}
}

// Called to bind functionality to input
void ADamageableCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ADamageableCharacter::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor == Target)
	{
		Target = nullptr;
	}
}

float ADamageableCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
	AController* EventInstigator, AActor* DamageCauser)
{
	if (DamageCauser->ActorHasTag(FName(FString::FromInt(EnemyTeam))))
	{
		Hurt(Damage);

		if (HealthPoints <= MinHealthPoints)
		{
			// Get the AI controller of the opposing team who gave the damage
			ATeamAIController* EnemyAIController = Cast<ATeamAIController>(EventInstigator);

			if (EnemyAIController)
			{
				// Reset the chase player component
				EnemyAIController->ResetChaseEnemy();
			}
			
			// Destroy actor
			Destroy();
		}
	}
	return 0.0f;
}

void ADamageableCharacter::SwitchTeams_Implementation(ETeams NewTeam)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Hacked Enemy"));

	// Removed old team tag
	Tags.Remove(FName(FString::FromInt(AllyTeam)));

	// Add new team tag
	AllyTeam = NewTeam;
	Tags.Add(FName(FString::FromInt(AllyTeam)));

	// Set enemy team to opposite of ally team
	switch (AllyTeam)
	{
	case ETeams::RED:
		EnemyTeam = ETeams::BLUE;
		break;
	case ETeams::BLUE:
		EnemyTeam = ETeams::RED;
		break;
	default:
		break;
	}

	// Updates the team colour
	InitialiseTeamColour();

	// Update controller class to stop chasing enemy 
	ATeamAIController* AIController = Cast<ATeamAIController>(GetController());

	if (AIController)
	{
		AIController->ResetChaseEnemy();
	}
}
