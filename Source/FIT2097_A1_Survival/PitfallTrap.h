// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "Kismet/GameplayStatics.h"
#include "FIT2097_A1_SurvivalCharacter.h"
#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PitfallTrap.generated.h"

UCLASS()
class FIT2097_A1_SURVIVAL_API APitfallTrap : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APitfallTrap();

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* BaseMesh;

	// Collision Propertiees
	UPROPERTY(EditAnywhere, Category = "Collision Box")
		UBoxComponent* TriggerVolume;

	UPROPERTY(EditAnywhere, Category = "Collision Box")
		FVector TriggerVolumeScale;

	UPROPERTY(EditAnywhere, Category = "Collision Box")
		FVector TriggerVolumeLocation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
