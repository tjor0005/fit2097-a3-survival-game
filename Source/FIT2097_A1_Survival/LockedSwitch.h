// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ItemKey.h"
#include "CoreMinimal.h"
#include "Switch.h"
#include "LockedSwitch.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API ALockedSwitch : public ASwitch
{
	GENERATED_BODY()
	
public:
	ALockedSwitch();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Key")
		AItemKey* Key;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// This is the function that will be called when Interact event is called on this object
	virtual int InteractInventory_Implementation(class UInventoryComponent* InventoryComponent) override;

};
