// Fill out your copyright notice in the Description page of Project Settings.


#include "AIWanderController.h"

AAIWanderController::AAIWanderController()
{

}

void AAIWanderController::GenerateNewRandomLocation()
{
	// Checks if navigation system has been made
	if (NavigationSystem)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Navigation System Set"));

		FNavLocation ReturnLocation;
		// gives us a random point in a radius that AI is able to be navigated towards
		NavigationSystem->GetRandomPointInNavigableRadius(GetPawn()->GetActorLocation(), 2000, ReturnLocation);

		// send location to blackboard
		BlackboardComponent->SetValueAsVector("PatrolPoint", ReturnLocation.Location);
		// Move to that location
		//MoveToLocation(ReturnLocation.Location);
	}
}