// Fill out your copyright notice in the Description page of Project Settings.


#include "Fuse.h"

AFuse::AFuse()
{
	// hard code mesh
	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObject(TEXT("/Game/StarterContent/Shapes/Shape_NarrowCapsule.Shape_NarrowCapsule"));

	if (MeshObject.Succeeded())
	{
		BaseMesh->SetStaticMesh(MeshObject.Object);
	}

	// initialise variables
	Name = FText::FromString("Fuse");
	Description = FText::FromString("Can be inserted into a power box to give power");
	HowTo = FText::FromString("Press E to pickup");

	IsConsumable = false;
}

void AFuse::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	if (matInstance)
	{
		TimerCounter += DeltaTime * Speed * Direction;
		if (TimerCounter >= 1)
		{
			TimerCounter = 1;
			Direction = -Direction;
		}
		else if (TimerCounter <= 0)
		{
			TimerCounter = 0;
			Direction = -Direction;
		}
		FLinearColor LerpColour = FMath::Lerp(OnColour, OffColour, TimerCounter);
		matInstance->SetVectorParameterValue("Colour", LerpColour);
	}
}

void AFuse::BeginPlay()
{
	Super::BeginPlay();

	if (matInstance)
	{
		matInstance->SetScalarParameterValue("Metallic", 1.0f);
	}
}
