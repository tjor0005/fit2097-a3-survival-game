// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TeamSwitchable.h"
#include "CoreMinimal.h"
#include "Grenade.h"
#include "HackGrenade.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API AHackGrenade : public AGrenade
{
	GENERATED_BODY()
	
public:
	AHackGrenade();

	// Ally Team to convert enemies to
	UPROPERTY(VisibleAnywhere, Category = "Tags")
		TEnumAsByte<ETeams> AllyTeam = ETeams::BLUE;

	void HackEnemies();

	void SpawnGrenade();

	void Explode();
};
