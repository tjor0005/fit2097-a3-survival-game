// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "HealthPickup.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API AHealthPickup : public APickup
{
	GENERATED_BODY()
	
public:

	AHealthPickup();

	// denote amount of health to give player
	UPROPERTY(EditAnywhere)
		float HealthGiven = 0.1f;

	virtual bool UseItem() override;
};
