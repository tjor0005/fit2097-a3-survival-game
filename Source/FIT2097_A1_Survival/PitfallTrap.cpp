// Fill out your copyright notice in the Description page of Project Settings.


#include "PitfallTrap.h"

// Sets default values
APitfallTrap::APitfallTrap()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	BaseMesh->SetCollisionProfileName(TEXT("Trigger"));
	RootComponent = BaseMesh;

	// Setup the collision box
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Volume"));
	TriggerVolume->SetupAttachment(RootComponent);

	// Setup relative location and world scale of collision box
	TriggerVolumeScale = FVector(3, 3, 0.1);
	TriggerVolumeLocation = FVector(0, 0, 8);
	TriggerVolume->SetWorldScale3D(TriggerVolumeScale);
	TriggerVolume->SetWorldLocation(TriggerVolumeLocation);
}

// Called when the game starts or when spawned
void APitfallTrap::BeginPlay()
{
	Super::BeginPlay();
	
	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &APitfallTrap::OnBoxOverlapBegin);

}

void APitfallTrap::OnBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// checks if the other actor exists and if it is not this actor
	if (OtherActor && (OtherActor != this) && OtherComponent)
	{
		// checks if other actor is a player character
		AFIT2097_A1_SurvivalCharacter* Player = Cast<AFIT2097_A1_SurvivalCharacter>(OtherActor);
		if (Player)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("Player has been killed by a pitfall trap"));
			// run playerLose function to make the player lose the game
			Player->PlayerLose();
		}
	}
}

// Called every frame
void APitfallTrap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

