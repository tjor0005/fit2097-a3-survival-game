// Fill out your copyright notice in the Description page of Project Settings.


#include "SwitchActivatedObject.h"

// Sets default values
ASwitchActivatedObject::ASwitchActivatedObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// setup mesh component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	BaseMesh->SetupAttachment(RootComponent);

	// setup initial mesh
	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObject(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));
	// hard code material
	ConstructorHelpers::FObjectFinder<UMaterial> MaterialObject(TEXT("/Game/_MyContent/Materials/Metal.Metal"));

	if (MeshObject.Succeeded())
	{
		BaseMesh->SetStaticMesh(MeshObject.Object);
	}

	if (MaterialObject.Succeeded())
	{
		Material = MaterialObject.Object;
		BaseMesh->SetMaterial(0, Material);
	}
}

// Called when the game starts or when spawned
void ASwitchActivatedObject::BeginPlay()
{
	Super::BeginPlay();

	Destination = OriginLocation;

	// checks if switch vairable has been initialised
	if (Switch)
	{
		// attach operate event to class OperateObject
		Switch->Operate.AddDynamic(this, &ASwitchActivatedObject::OperateObject);
	}

	// initialise dynamic material instance and set it as mesh material
	if (Material)
	{
		matInstance = UMaterialInstanceDynamic::Create(Material, this);
		BaseMesh->SetMaterial(0, matInstance);
		matInstance->SetScalarParameterValue("Emissive", OffEmissive);
	}
}


// Called every frame
void ASwitchActivatedObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Calculate the current direction of the actor to the destination
	FVector CurrentDirection = Destination - GetActorLocation();
	CurrentDirection.Normalize();

	// Check if the distance between actor and destination is within tolerance or if actor has moved past the destination location
	if (FVector::Dist(GetActorLocation(), Destination) <= DistTolerance || FVector::DotProduct(TargetDirection, CurrentDirection) <= 0)
	{
		// set actor location to destination
		SetActorLocation(Destination);
	}
	else
	{
		SetActorLocation(FMath::Lerp(GetActorLocation(), Destination, MovementRate));
	}

	// Checks if the FVectors have been defined
	if (!OriginLocation.IsZero() && !TargetLocation.IsZero())
	{
		// Moves the actor to target location if activated
		if (activated)
		{
			SetActorLocation(FMath::Lerp(GetActorLocation(), TargetLocation, MovementRate));
		}
		// Moves the actors to origin location if activated
		else
		{
			SetActorLocation(FMath::Lerp(GetActorLocation(), OriginLocation, MovementRate));
		}
	}
}

void ASwitchActivatedObject::OperateObject(bool Activation)
{
	//GEngine->AddOnScreenDebugMessage(-1, 0.01f, FColor::Red, TEXT("OBJECT HAS BEEN SWITCHED ON"));
	
	activated = !activated;

	if (activated)
	{
		Destination = TargetLocation;
		matInstance->SetScalarParameterValue("Emissive", OnEmissive);
	} 
	else
	{
		Destination = OriginLocation;
		matInstance->SetScalarParameterValue("Emissive", OffEmissive);
	}

	// Update DirectionVector
	FVector DirectionVector = Destination - GetActorLocation();
	DirectionVector.Normalize();
	TargetDirection = DirectionVector;
}