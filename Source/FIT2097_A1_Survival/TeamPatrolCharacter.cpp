// Fill out your copyright notice in the Description page of Project Settings.


#include "TeamPatrolCharacter.h"

ATeamPatrolCharacter::ATeamPatrolCharacter()
{

}

void ATeamPatrolCharacter::InCurrentCheckpoint()
{
	CurrentCheckpoint = (CurrentCheckpoint + 1) % PatrolPoints.Num();
}

void ATeamPatrolCharacter::BeginPlay()
{
	Super::BeginPlay();

	// initialise patrol points
	if (PatrolPoints.Num() > 0)
	{
		// set initial checpoint to current actor location
		PatrolPoints[0] = GetActorLocation();
	}
	else if (PatrolPoints.Num() == 0)
	{
		PatrolPoints.Add(GetActorLocation());
	}
}
