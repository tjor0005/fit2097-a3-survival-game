// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "FIT2097_A1_SurvivalHUD.generated.h"

UCLASS()
class AFIT2097_A1_SurvivalHUD : public AHUD
{
	GENERATED_BODY()

public:
	AFIT2097_A1_SurvivalHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

