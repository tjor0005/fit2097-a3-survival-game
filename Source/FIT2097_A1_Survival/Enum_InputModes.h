// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enum_InputModes.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum EInputModes
{
	GAME_ONLY = 0 UMETA(DisplayName = "Game"),
	UI_ONLY = 1 UMETA(DisplayName = "UI"),
	GAME_UI = 2 UMETA(DisplayName = "Game and UI"),
};