// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "FoodPickup.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API AFoodPickup : public APickup
{
	GENERATED_BODY()
	
public:
	AFoodPickup();

	// denote the amount of food levels to give
	UPROPERTY(EditAnywhere)
		float FoodGiven = 0.1f;

	virtual bool UseItem() override;
};
