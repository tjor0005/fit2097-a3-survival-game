// Copyright Epic Games, Inc. All Rights Reserved.

#include "FIT2097_A1_SurvivalGameMode.h"
#include "FIT2097_A1_SurvivalHUD.h"
#include "FIT2097_A1_SurvivalCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFIT2097_A1_SurvivalGameMode::AFIT2097_A1_SurvivalGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFIT2097_A1_SurvivalHUD::StaticClass();
}
