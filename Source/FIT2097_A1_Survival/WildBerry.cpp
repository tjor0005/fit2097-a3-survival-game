// Fill out your copyright notice in the Description page of Project Settings.


#include "WildBerry.h"

AWildBerry::AWildBerry()
{
	// initialise item description
	Name = FText::FromString("Wild Berry");
	Description = FText::FromString("Consume to get random attribute boost, if you dare");

	// Initialise material color
	OnColour = FLinearColor(0.262423, 0, 0.369792);

	// initialise Thumbnail
	ConstructorHelpers::FObjectFinder<UTexture2D> Texture(TEXT("/Game/_MyContent/Thumbnails/PNG_WildBerry.PNG_WildBerry"));

	if (Texture.Succeeded())
	{
		Thumbnail = Texture.Object;
	}
}

bool AWildBerry::UseItem()
{
	bool res = Super::UseItem();

	if (PlayerTarget)
	{
		PlayerTarget->AddDrunkEffect(Duration);

		// Randomly increase one of the three player attributes
		int value = FMath::RandRange(0,2);
		switch (value)
		{
		case 0:
			PlayerTarget->Heal(AttributeBoost);
			break;
		case 1:
			PlayerTarget->IncreaseFood(AttributeBoost);
			break;
		case 2:
			PlayerTarget->IncreaseWater(AttributeBoost);
			break;
		default:
			break;
		}
	}

	return res;
}