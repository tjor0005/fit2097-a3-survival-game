// Fill out your copyright notice in the Description page of Project Settings.


#include "HackGrenade.h"

AHackGrenade::AHackGrenade()
{
	// Change item description
	Name = FText::FromString("Hack Grenade");
	Description = FText::FromString("Converts enemies into allies");
}

void AHackGrenade::HackEnemies()
{
	for (int i = 0; i < SurroundingEnemies.Num(); i++)
	{
		// Checks if surrounding enemy implements Stunnable Interface
		if (SurroundingEnemies[i]->GetClass()->ImplementsInterface(UTeamSwitchable::StaticClass()))
		{
			ITeamSwitchable::Execute_SwitchTeams(SurroundingEnemies[i], AllyTeam);
		}
		//SurroundingEnemies[i]->Stun(StunTime);
	}
}

void AHackGrenade::SpawnGrenade()
{
	// Get current location and look at rotation of player
	FVector PlayerLocation = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetPawnViewLocation();
	FRotator PlayerRotation = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetViewRotation();

	// spawns another StunGrenade actor in world in front of player
	AHackGrenade* tempReference = GetWorld()->SpawnActor<AHackGrenade>(AHackGrenade::StaticClass(),
		PlayerLocation + ThrowOffset, PlayerRotation);

	// throw the grenade
	tempReference->ThrowGrenade();
}

void AHackGrenade::Explode()
{
	// run explosion effects
	RunExplodeFX();

	// Stun enemies
	HackEnemies();

	// Destroy Item
	Destroy();
}
