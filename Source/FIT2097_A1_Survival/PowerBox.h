// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LockedSwitch.h"
#include "PowerBox.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API APowerBox : public ALockedSwitch
{
	GENERATED_BODY()

public:
	APowerBox();
	
	// This is the function that will be called when Interact event is called on this object
	virtual int InteractInventory_Implementation(class UInventoryComponent* InventoryComponent) override;
};
