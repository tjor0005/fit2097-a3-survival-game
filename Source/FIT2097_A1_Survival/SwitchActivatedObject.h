// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Switch.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SwitchActivatedObject.generated.h"

// this is our forward declaration
class ASwitch;

UCLASS()
class FIT2097_A1_SURVIVAL_API ASwitchActivatedObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASwitchActivatedObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UStaticMeshComponent* BaseMesh;

	// Object Material
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		UMaterialInterface* Material;

	UMaterialInstanceDynamic* matInstance;

	// Object Material
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		float OnEmissive = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		float OffEmissive = 0.0f;

	// start location of actor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FVector OriginLocation;

	// target location of actor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FVector TargetLocation;

	// rate at which actor moves
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MovementRate = 0.05f;

	// signifies when the actor has moved or not
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Switch")
		bool activated = false;

	// reference to switch class to create handler event
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Switch")
		ASwitch* Switch;

	// Distance tolerance
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float DistTolerance = 10.0f;

	// Initial Direction from actor to target
	FVector TargetDirection = FVector(0, 0, 0);

	// Location of the Destination
	FVector Destination = FVector(0, 0, 0);

	UFUNCTION()
		void OperateObject(bool Activation);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
