// Fill out your copyright notice in the Description page of Project Settings.


#include "MultipleSwitchActivatedObject.h"

// Sets default values
AMultipleSwitchActivatedObject::AMultipleSwitchActivatedObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// setup mesh component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	BaseMesh->SetupAttachment(RootComponent);

	// setup initial mesh
	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObject(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));
	// hard code material
	ConstructorHelpers::FObjectFinder<UMaterial> MaterialObject(TEXT("/Game/_MyContent/Materials/Metal.Metal"));

	if (MeshObject.Succeeded())
	{
		BaseMesh->SetStaticMesh(MeshObject.Object);
	}

	if (MaterialObject.Succeeded())
	{
		Material = MaterialObject.Object;
		BaseMesh->SetMaterial(0, Material);
	}
}

void AMultipleSwitchActivatedObject::OperateObject(ASelfSwitch* SwitchActor, bool Activation)
{
	SwitchActivationStates.Emplace(SwitchActor, Activation);
	UpdateStatus();
}

void AMultipleSwitchActivatedObject::UpdateStatus()
{
	bool allActivated = true;
	for (auto& Elem : SwitchActivationStates)
	{
		if (!Elem.Value)
		{
			allActivated = false;
		}
	}

	if (allActivated)
	{
		TurnedOn = true;
		Destination = TargetLocation;

		// modify emissive material
		if (matInstance)
		{
			matInstance->SetScalarParameterValue("Emissive", OnEmissive);
		}
	}
	else
	{
		TurnedOn = false;
		Destination = OriginLocation;

		// modify emissive material
		if (matInstance)
		{
			matInstance->SetScalarParameterValue("Emissive", OffEmissive);
		}
	}

	// Update DirectionVector
	FVector DirectionVector = Destination - GetActorLocation();
	DirectionVector.Normalize();
	TargetDirection = DirectionVector;
}

// Called when the game starts or when spawned
void AMultipleSwitchActivatedObject::BeginPlay()
{
	Super::BeginPlay();

	Destination = OriginLocation;

	// initialise dynamic material instance and set it as mesh material
	if (Material)
	{
		matInstance = UMaterialInstanceDynamic::Create(Material, this);
		BaseMesh->SetMaterial(0, matInstance);
		matInstance->SetScalarParameterValue("Emissive", OffEmissive);
	}

	// Checks if Switches has been initialised
	if (Switches.Num() > 0)
	{
		// initialise SwitchActivationStates to false (not activated)
		for (int i = 0; i < Switches.Num(); i++)
		{
			ASelfSwitch* Switch = Switches[i];
			if (Switch)
			{
				Switch->OperateObject.AddDynamic(this, &AMultipleSwitchActivatedObject::OperateObject);
				SwitchActivationStates.Add(Switch, false);
			}
		}
	}
}

// Called every frame
void AMultipleSwitchActivatedObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Calculate the current direction of the actor to the destination
	FVector CurrentDirection = Destination - GetActorLocation();
	CurrentDirection.Normalize();

	// Check if the distance between actor and destination is within tolerance or if actor has moved past the destination location
	if (FVector::Dist(GetActorLocation(), Destination) <= DistTolerance || FVector::DotProduct(TargetDirection, CurrentDirection) <= 0)
	{
		// set actor location to destination
		SetActorLocation(Destination);
	}
	else
	{
		SetActorLocation(FMath::Lerp(GetActorLocation(), Destination, MovementRate));
	}
}

