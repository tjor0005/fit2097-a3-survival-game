// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Displayable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UDisplayable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class FIT2097_A1_SURVIVAL_API IDisplayable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		FText TextToDisplay();
};
