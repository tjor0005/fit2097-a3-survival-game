// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ItemKey.h"
#include "Fuse.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A1_SURVIVAL_API AFuse : public AItemKey
{
	GENERATED_BODY()

public:
	AFuse();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		FLinearColor OffColour = FLinearColor(1, 1, 1);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Material)
		float Speed = 0.5f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Material)
		float TimerCounter = 0.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Material)
		int Direction = 1;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
