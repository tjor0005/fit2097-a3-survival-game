How to open and start game:
 TO PLAY SURVIVAL MAP
 - Navigate to SurvivalMapv2.umap found in _MyContents -> Maps
 - Press Play
 TO GO TO DYNAMIC MATERIAL SHOWCASE
 - Nagivate to DynamicMaterialShowcase found in _MyContents -> Maps

How to watch demo video:
 - You can click on this link to get access to the video on google drive 
https://drive.google.com/file/d/1GdEKGHS-bTqqmRv_YpyN9uUgx0r86K7O/view?usp=sharing

Where to find design documentation:
 - Open the design folder to see pdf of UML diagrams and design explanation
 - 'DesignExplanation.txt' provides detailed information on the approach of the game design
 - 'General UML Interaction.pdf' is a UML interaction diagram of all the classes in the game
 - The rest of the pdf files are UML class diagrams that give a more information about the methods
   and variables in each of the classes

How to play:
 - When sequence is playing, click left mouse button to skip sequence
 - Press 'E' to interact with interactable objects in the world
 - press 'Tab' to see and interact inventory
	- Right mouse click the items to use 
 - press 'P' to pause and unpause the game
 - Hover over interactable objects to see their information and how to interact with them
 - Items like keys, fuses and pickups are stored in the inventory. You will need a specific key or fuse
   in your inventory to unlock certain switches or powerboxes in the game

Extra functionalities:
 - Team AI:
	- AIs are split into red teams and blue teams
	- Each team has their own correspodning material to distinguish them
	- AI characters that are in opposite teams attack one another if they are spotted
	- AI character attack by touching the enemy and slowly draining their health
 - Enemies drop a pickup item when they die
 - Fleshed out inventory system:
	- Acquire items and store them in inventory
	- capable to stacking items
	- Right click over a an inventory slot item to use it
	- Hover over item slots to display item name and description
 - More items
	- Stun grenade: Stuns enemy team AI for a certain amount of time
	- Hack grenade: Hacks the enemy team AI and converts them into an ally that attacks
	  enemies
	- Wild berry: consume the item to get a random attribute boost, but get drunk effect
	  for a certain amount of time
 - Extra fx
	- Added damage sound fx
	- Added explosion sound fx and explosion particles using Niagara particles
 - Pickup items are now stored in inventory

Functionalities to the game:
 - health drops at a constant rate when either drink or food level is 0
    - the health drop rate doubles when both drink and food levels are at 0
 - The player finishes the game by standing on top of the endplatform found on 
   the very end of the level
 - There are different types of enemy characters:
	- An enemy that randomly wanders around the stage
	- An enemy that patrols around predestined checkpoints around a fuse
 - Both of these enemies follow the player if they are within sight range and drain the 
   player's health when they are touching
 - You must use the covers in the stages to hide from the enemy and break off their pursuit

Additional information:
 - BP child classes were created from the c++ classes only to initialise the attributes 
   of the actors (i.e. Materials, StaticMeshes, Blackboard, Behaviour Tree, etc)
 - There is no logic that is contained within the BP child classes

Information about classes:
 BP_SurvivalGameMode (found in _MyContent -> BP)
 - This gamemode is used within the SurvivalMap.umap level and is used to initalise
   the hud widgets as well as the SurvivalGameState class with their necessary variables
 - It also acts as an event handler for SurvivalGameState, in which it will switch to 
   appropriate game screens when game state has changed
 SurvivalGameState
 - This game state is responsible for keeping track of the current player game state and making
   changes to the game when game state changes
 - It utilises the Enum_GameState Enumerator class to distinguish between different game states
   and responf appropriately to them
 Enum_GameStates
 - An enumerator class that is solely used to distinguish different game states
 - For some reason, the class cannot be found in the editor, but when navigating in visual studios
   it can be located within Source -> FIT2091_A1_Survival folder
 Displayable Interface
 - This interface is utilised to display information in the player playHUD screen when the 
   player is hovering over the actor that is implementing this interface
 Interactable Interface
 - This interface is to allow the player to interact with certain objects and make them
   perform a predetermined action without the player knowing information about that actor
 Obtainable Interface
 - This interface is used mainly for items that the player is able to pickup and store in their
   inventory
 - It allows the player character to obtain specific information in regards to an item that implements
   this interface and store this information within their inventory.
 - In this case, the information is the item description
InteractableSurvival Interface
 - Acts similar to the Interactable interface where it allows the player to interact with actors
 - The only difference is that it gives the option of viewing and modifying the player inventory
 - This is helpful for actors that need to check the player inventory and check if they have 
   a certain item/key/fuse in order to activate the actor (i.e. Powerbox and Switches)
 Locked Switch
 - This actor functions by having the specific player character and the KeyItem actors as variables
 - This is so that the actor can check if the player has an item in their inventory that is exactly
   the same as the unique keypattern found in the itemKey actor
 - the keypattern here is just the description of the item
 - For this function to work as intended, all of the items within the game must have unique item 
   descriptions
 SelfSwitches
 - Implements Interactable and Displayable that allows the player to interact with actor
   and view information about actor
 - Functions similar to Switches but it directly activates other SelfSwitches
 - Contains two event dispatchers: 
	- OperateSwitches: SelfSwitch is an event contains an event dispatcher and listener to this event.
			   This enables the function of self switches operating/activating other self
			   switches.
	- OperateObject: This is used for the MultipleSwitchActivatedObject and informs any listeners
			 what the activation state of the SelfSwitch is.
 MultipleSwitchActivatedObject:
 - Functions the same as SwitchActivatedObject but has a listener event for a list of SelfSwitches
 - This listener event for OperateObject checks the activation states of all the SelfSwitches it is 
   listening to and determines if all of them are activated or not. If so the 
   MultipleSwitchActivatedObject activates
EnemyCharacter:
 - extends from the Character class, this is the actor that will be used by the EnemyAIWanderController
 - It is able to drain the player health when they are within a certain radius. In the assignment,
   this radius is within touching distance
 - The speed is modified to be 90% of the player speed
 - contains a UTextRenderComponent that shows up above the enemy character when it is alerted.
 - This functionality is used by the AI controllers to show that the enemy has detected the player
 - Uses EnemyAIWanderController for movement and navigation
EnemyPatrolCharacter:
 - extends from EnemyCharacter class
 - contains attributes that are needed for the enemy to patrol around checkpoints
 - These attributes are used by EnemyAIPatrollerController class to determine what checkpoint
   the enemy is able to go to next.
EnemyAIWanderController:
 - extends from Controller class
 - Defines the main sense configuration of the enemy which is sight
 - Contains all the logic for the EnemyCharacter which includes:
	- randomly navigate around the NavMeshBoundsVolume
	- Determine if the player character is within the enemy sight radius
	- follow the player while they are within their sight
EnemyAIPatrolContrller class:
 - extends from Controller class
 - Defines the main sense configuration of the enemy which is sight
 - Contains all the logic for the EnemyPatrolCharacter which includes:
	- Patrolling around pre-defined checkpoints
	- Determine if the player character is within the enemy sight radius
	- follow the player while they are within their sight
GeneratePatrolPointTask:
 - Used by the EnemyPatrolBT to generate next patrol location for the AI enemy to move to
GenerateRandomLocationTask:
 - Used by the EnemyWanderBT to generate next random location for the AI enemy to move to
EnemyBlackboard:
 - Defines variables that are used by EnemyPatrolBY and EnemyWanderBT to make them function
   porperly
EnemyPatrolBT:
 - Behaviour Tree that is used by MyEnemyAIPatrolController class
 - Contains logic that gives directions for the Enemy AI to follow:
	- check if they can see the player. Follow the player if so
	- if not, patrol around checkpoints
EnemyWanderBT:
 - A Behaviour Tree that is used bt MyEnemyAIWanderController class
 - Contains logic that gives directions for the Enemy AI to follow:
	- check if they can see the player. Follow the player if so
	- if not, randomly wander around navigational mesh
